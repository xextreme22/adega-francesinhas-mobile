package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Allergen;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class AllergenAdaptar extends RecyclerView.Adapter<AllergenAdaptar.ViewHolder> {
    private List<Allergen> allergens;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName;
        public CircleImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }

    public AllergenAdaptar(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    @NonNull
    @Override
    public AllergenAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_allergen, parent, false);
        return new AllergenAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllergenAdaptar.ViewHolder holder, int position) {
        Allergen allergen = allergens.get(position);

        Picasso.get().load(allergen.getUri()).into(holder.imageView);
        holder.textViewName.setText(allergen.getName());
    }

    @Override
    public int getItemCount() {
        return allergens.size();
    }
}
