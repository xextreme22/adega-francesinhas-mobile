package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RestaurantSchedule {

    private List<Calendar> listOpenedDates;
    private List<ScheduleTimes> listOpenedTimes;

    public RestaurantSchedule() {
        listOpenedDates = new ArrayList<>();
        listOpenedTimes = new ArrayList<>();
    }

    public List<Calendar> getListOpenedDates() {
        return listOpenedDates;
    }

    public List<ScheduleTimes> getListOpenedTimes() {
        return listOpenedTimes;
    }

    private int convertDatabaseFormatToCalendarFormat(String databaseWeekDay){
        switch (databaseWeekDay){
            case "domingo":
                return 1;
            case "segunda":
                return 2;
            case "terca":
                return 3;
            case "quarta":
                return 4;
            case "quinta":
                return 5;
            case "sexta":
                return 6;
            case "sabado":
                return 7;
            default:
                return -1;
        }
    }

    public void addDates(Schedule schedule){
        int formatedWeekDay = convertDatabaseFormatToCalendarFormat(schedule.getWeekDay());
        if(formatedWeekDay == -1){
            return;
        }

        Calendar openDate = Calendar.getInstance();
        openDate.set(Calendar.HOUR_OF_DAY, (schedule.getOpenHour() / 100));
        openDate.set(Calendar.MINUTE, (schedule.getOpenHour() % 100));

        Calendar closeDate = Calendar.getInstance();
        closeDate.set(Calendar.HOUR_OF_DAY, (schedule.getCloseHour() / 100));
        closeDate.set(Calendar.MINUTE, (schedule.getCloseHour() % 100));

        Calendar oneMonthFromNow = Calendar.getInstance();
        oneMonthFromNow.add(Calendar.MONTH, 1);

        while (openDate.compareTo(oneMonthFromNow) < 0) {
            if(openDate.get(Calendar.DAY_OF_WEEK) == formatedWeekDay){
                addToListCalendarDatesSimplified((Calendar) openDate.clone(), (Calendar) closeDate.clone());
            }
            openDate.add(Calendar.DAY_OF_YEAR, 1);
            closeDate.add(Calendar.DAY_OF_YEAR, 1);
        }
    }

    public void addToListCalendarDatesSimplified(Calendar openDate, Calendar closeDate){ //dates are always at the same day
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, 15);

        Calendar aux = openDate;
        if(sameDay(currentCalendar, openDate)){
            if(currentCalendar.compareTo(closeDate) > 0){ //se a data corrente for maior que a data de fecho entao nao adicionamos nada
                return;
            }else if(currentCalendar.compareTo(openDate) >= 1){ //se a data corrente for menor que a data de fecho e maior que a data de abertura entao usamos a data currente como ponto incial
                aux = (Calendar) currentCalendar.clone();
            }else{
                return;
            }
        }

        int position = getPositionListCalendars(aux);
        if(position == -1){ //if date doesn't exist yet, add it and its respective times
            Calendar calendarClone = (Calendar) aux.clone();
            listOpenedDates.add(calendarClone);
            position = listOpenedDates.indexOf(calendarClone);
            listOpenedTimes.add(position, new ScheduleTimes());
            listOpenedTimes.get(position).addTimepointsBetween((Calendar) aux.clone(), (Calendar) closeDate.clone());
        }else{ //add new times to existing date
            listOpenedTimes.get(position).addTimepointsBetween((Calendar) aux.clone(), (Calendar) closeDate.clone());
        }
    }

    public void addToListCalendarDates(Calendar openDate, Calendar closeDate){
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, 15);

        int aux= 0;
        //for schedules with diferente dates for opening and closing (when restaurante is open for example 48h straight)
        while(openDate.compareTo(closeDate) != 1){ //while openDate is befor closeDate
            if(currentCalendar.compareTo(openDate) == 1){ //se data currente for superior a data de abertura
                if(!sameDay(currentCalendar, openDate)){ //if current date is superior to openDate and not in the same day
                    openDate.add(Calendar.DAY_OF_MONTH, 1);
                    aux++;
                    continue;
                }
            }

            int position = listPostionCalendar(openDate);
            if(position == -1){
                Calendar calendarClone = (Calendar) openDate.clone();
                listOpenedDates.add(calendarClone);
                position = listOpenedDates.indexOf(calendarClone);
                listOpenedTimes.add(position, new ScheduleTimes());
            }

            if(aux==0){ //1st iteration always type data to 00:00 or to data
                if(sameDay(openDate, closeDate)){ //se primeira iteração e mesmo dia entao é data to data
                    if(currentCalendar.compareTo(openDate) == 1){ //se data currente for superior a data de abertura
                        listOpenedTimes.get(position).addTimepointsBetween((Calendar) currentCalendar.clone(), (Calendar) closeDate.clone());
                    }else{
                        listOpenedTimes.get(position).addTimepointsBetween((Calendar) openDate.clone(), (Calendar) closeDate.clone());
                    }
                }else{ //data to 00:00
                    if(currentCalendar.compareTo(openDate) == 1){
                        listOpenedTimes.get(position).addTimepointsUntilEndDay((Calendar) currentCalendar.clone());
                    }else{
                        listOpenedTimes.get(position).addTimepointsUntilEndDay((Calendar) openDate.clone());
                    }
                }
            }else{
                if(sameDay(openDate, closeDate)){ //if we reached the end its type 00:00 to data
                    if(sameDay(currentCalendar, openDate)){
                        listOpenedTimes.get(position).addTimepointsBetween((Calendar) currentCalendar.clone(), (Calendar) closeDate.clone());
                    }else {
                        listOpenedTimes.get(position).addTimepointsFromStartDay((Calendar) closeDate.clone());
                    }
                }else{ //if we haven't reached the end and it isn't the 1st iteration then its type 00:00 to 00:00
                    if(sameDay(currentCalendar, openDate)){
                        listOpenedTimes.get(position).addTimepointsUntilEndDay((Calendar) currentCalendar.clone());
                    }else {
                        listOpenedTimes.get(position).addTimepointsCompleteDay();
                    }
                }
            }

            openDate.add(Calendar.DAY_OF_MONTH, 1);
            aux++;
        }
    }

    public void addClosedExceptions(Calendar openDate, Calendar closeDate){
        List<Timepoint> times;
        int position;
        int aux = 0;
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, 15);

        if(sameDay(openDate, closeDate)){
            position = listPostionCalendar(openDate);
            if(position != -1){
                times =  listOpenedTimes.get(position).timepointsNotBetween(ScheduleTimes.timepoint(openDate), ScheduleTimes.timepoint(closeDate));
                if(times.size() == 0){
                    listOpenedTimes.remove(position);
                    listOpenedDates.remove(position);
                }else{
                    listOpenedTimes.get(position).removeTimepointsBetween((Calendar) openDate.clone(), (Calendar) closeDate.clone());
                }
            }
            return;
        }

        while(openDate.compareTo(closeDate) != 1){ //while openDate is before closeDate
            if(currentCalendar.compareTo(openDate) == 1 && !sameDay(currentCalendar, openDate)){ //se data currente for superior a data de abertura //if current date is superior to openDate and not in the same day
                openDate.add(Calendar.DAY_OF_MONTH, 1);
                aux++;
                continue;
            }

            position = listPostionCalendar(openDate);
            if(position == -1){ //se nao tiver nao vale a pena remover
                openDate.add(Calendar.DAY_OF_MONTH, 1);
                aux++;
                continue;
            }

            if(aux==0){ //1st iteration always type open date to 23:55 cannot be data to data cause its done before
                times =  listOpenedTimes.get(position).timepointsNotBetween(ScheduleTimes.timepoint(openDate), new Timepoint(23,55));
                if(times.size() == 0){
                    listOpenedTimes.remove(position);
                    listOpenedDates.remove(position);
                }else{
                    listOpenedTimes.get(position).removeTimepointsUntilEndDay((Calendar) openDate.clone());
                }
            }else{
                if(sameDay(openDate, closeDate)){ //if we reached the end its type 00:00 to data
                    times =  listOpenedTimes.get(position).timepointsNotBetween(new Timepoint(0,0), ScheduleTimes.timepoint(closeDate));
                    if(times.size() == 0){
                        listOpenedTimes.remove(position);
                        listOpenedDates.remove(position);
                    }else{
                        listOpenedTimes.get(position).removeTimepointsFromStartDay((Calendar) closeDate.clone());
                    }
                }else{ //if we haven't reached the end and it isn't the 1st iteration then its type 00:00 to 23:55
                    listOpenedTimes.remove(position);
                    listOpenedDates.remove(position);
                }
            }

            openDate.add(Calendar.DAY_OF_MONTH, 1);
            aux++;
        }

        position = listPostionCalendar(openDate);
        if(position == -1){ //se nao tiver nao vale a pena remover
            openDate.add(Calendar.DAY_OF_MONTH, 1);
            aux++;
            return;
        }
        if(sameDay(openDate, closeDate)){ //acontece quando o tempo da data de abertura e superior ao da data de fim e falta remover entao de 00:00 until endDate
            if(openDate.compareTo(closeDate) != 0){
                listOpenedTimes.get(position).removeTimepointsFromStartDay((Calendar) closeDate.clone());
            }
        }

        return;
    }

    public Boolean sameDay (Calendar calendar1, Calendar calendar2){
        return (calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR) && calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR));
    }

    public int listPostionCalendar (Calendar calendar1){
        for (Calendar calendar : listOpenedDates){
            if(sameDay(calendar, calendar1)){
                return listOpenedDates.indexOf(calendar);
            }
        }
        return -1;
    }


    public int getPositionListCalendars (Calendar calendarDay){
        int position = 0;
        for (Calendar calendar: listOpenedDates) {
            if(calendar.get(Calendar.DAY_OF_YEAR) == calendarDay.get(Calendar.DAY_OF_YEAR) && calendar.get(Calendar.YEAR) == calendarDay.get(Calendar.YEAR)){
                return position;
            }
            position++;
        }
        return -1;
    }
}
