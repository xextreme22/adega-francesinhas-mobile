package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.menus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Menu;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations.ReservationOrderActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways.TakeawayOrderActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class OrderMenuActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;
    private UserInfoInstance userInfoInstance;

    private String category;
    private String restaurant;

    private ArrayList<Menu> menus = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private Button buttonBack;
    private Button buttonFinalize;
    private TextView textViewTitle;
    private TextView textViewNoItems;
    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_recycler_button);

        category = getIntent().getStringExtra("category");

        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            restaurant = TakeawayOrderInstance.getInstance().getTakeaway().getRestaurante();
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            restaurant = ReservationOrderInstance.getInstance().getReservation().getRestaurante();
        }

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        buttonBack = findViewById(R.id.buttonBack);
        buttonFinalize = findViewById(R.id.buttonBottom);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        textViewNoItems.setText(getString(R.string.INFO_NO_MENUS));
        textViewTitle.setText(category);

        dialogLoading = DialogLoading.loadingDialog(this, getLayoutInflater());

        buttonFinalize.setText(getString(R.string.Finalize));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonFinalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFinalize.setEnabled(false);
                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    storeOrdersTakeaway();
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    storeOrdersReservation();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(OrderMenuActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(OrderMenuActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    buildRecyclerView();
                    if(menus.size() == 0){
                        dialogLoading.show();
                        loadMenus();
                    }
                }
            }else{
                Intent intent = new Intent(OrderMenuActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(OrderMenuActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new OrderMenuAdaptar(menus, OrderMenuActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void loadMenus(){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(restaurant)
                .collection(getString(R.string.DATABASE_CATEGORY_COLLECTION))
                .document(category)
                .collection(getString(R.string.DATABASE_ITEMS_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else {
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    String menuName = document.getId();
                                    Number price = (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD));
                                    String ingredients = document.get(getString(R.string.DATABASE_INGREDIENTS_FIELD)).toString();

                                    String ref = "restaurantes/menu/" + menuName + ".jpg";
                                    StorageReference itemRef = storageRef.child(ref);
                                    itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            menus.add(new Menu(uri, menuName, ingredients, restaurant, price, category));
                                            adapter.notifyDataSetChanged();
                                        }
                                    });
                                }
                            }
                                dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }

    private int aux;
    private void storeOrdersReservation(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                .document(ReservationOrderInstance.getInstance().getReservationId())
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(getString(R.string.DATABASE_QUANTITY_FIELD));
                    Order order = new Order(
                            quantidade.intValue(),
                            doc.getString(getString(R.string.DATABASE_MENU_FIELD)),
                            doc.getString(getString(R.string.DATABASE_CATEGORY_FIELD)),
                            doc.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(getString(R.string.DATABASE_PRICE_FIELD))
                    );
                    int pos = ReservationOrderInstance.getInstance().getPosition(order);
                    if(pos != -1){
                        int quantidade1 = ReservationOrderInstance.getInstance().getOrders().get(pos).getQuantidade();
                        if(quantidade1 != order.getQuantidade()){
                            ReservationOrderInstance.getInstance().getOrders().remove(pos);
                            db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                                    .document(ReservationOrderInstance.getInstance().getReservationId())
                                    .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).update(getString(R.string.DATABASE_QUANTITY_FIELD), quantidade1);
                        }else{
                            ReservationOrderInstance.getInstance().getOrders().remove(pos);
                        }
                    }else{
                        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                                .document(ReservationOrderInstance.getInstance().getReservationId())
                                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).delete();
                    }
                }
                addNewOrdersReservation();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
            }
        });
    }

    private void addNewOrdersReservation(){
        aux = 0;
        if(ReservationOrderInstance.getInstance().getOrders().size() == 0){
            Intent intent = new Intent(OrderMenuActivity.this, ReservationOrderActivity.class);
            startActivity(intent);
            return;
        }

        dialogLoading.show();
        for (Order order : ReservationOrderInstance.getInstance().getOrders()){
            db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                    .document(ReservationOrderInstance.getInstance().getReservationId())
                    .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).add(order).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    if(task.isSuccessful()){
                        aux++;
                        if(aux == ReservationOrderInstance.getInstance().getOrders().size()){
                            dialogLoading.dismiss();
                            Intent intent = new Intent(OrderMenuActivity.this, ReservationOrderActivity.class);
                            startActivity(intent);
                        }
                    }else{
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                }
            });
        }
    }

    private void storeOrdersTakeaway(){
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(getString(R.string.DATABASE_QUANTITY_FIELD));
                    Order order = new Order(
                            quantidade.intValue(),
                            doc.getString(getString(R.string.DATABASE_MENU_FIELD)),
                            doc.getString(getString(R.string.DATABASE_CATEGORY_FIELD)),
                            doc.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(getString(R.string.DATABASE_PRICE_FIELD))
                    );
                    int pos = TakeawayOrderInstance.getInstance().getPosition(order);
                    if(pos != -1){
                        int quantidade1 = TakeawayOrderInstance.getInstance().getOrders().get(pos).getQuantidade();
                        if(quantidade1 != order.getQuantidade()){
                            TakeawayOrderInstance.getInstance().getOrders().remove(pos);
                            db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                                    .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                                    .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).update(getString(R.string.DATABASE_QUANTITY_FIELD), quantidade1);
                        }else{
                            TakeawayOrderInstance.getInstance().getOrders().remove(pos);
                        }
                    }else{
                        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).delete();
                    }
                }
                addNewOrdersTakeaway();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
            }
        });
    }

    private void addNewOrdersTakeaway(){
        aux = 0;
        if(TakeawayOrderInstance.getInstance().getOrders().size() == 0){
            Intent intent = new Intent(OrderMenuActivity.this, TakeawayOrderActivity.class);
            startActivity(intent);
            return;
        }

        dialogLoading.show();
        for (Order order : TakeawayOrderInstance.getInstance().getOrders()){
            db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                    .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                    .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).add(order).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    if(task.isSuccessful()){
                        aux++;
                        if(aux == TakeawayOrderInstance.getInstance().getOrders().size()){
                            dialogLoading.dismiss();
                            Intent intent = new Intent(OrderMenuActivity.this, TakeawayOrderActivity.class);
                            startActivity(intent);
                        }
                    }else{
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                }
            });
        }
    }
}
