package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.previous;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;

public class PreviousAdaptar extends RecyclerView.Adapter<PreviousAdaptar.ViewHolder> {
    private List<List<Order>> orders;
    private Context context;
    private LayoutInflater layoutInflater;

    @Override
    public void onBindViewHolder(@NonNull PreviousAdaptar.ViewHolder holder, int position) {
        List<Order> currentOrder = orders.get(position);

        holder.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        PreviousOrderAdaptar adapter = new PreviousOrderAdaptar(currentOrder, context, layoutInflater);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.recyclerView.setAdapter(adapter);
    }

    public PreviousAdaptar(List<List<Order>> orders, Context context, LayoutInflater layoutInflater) {
        this.orders = orders;
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @NonNull
    @Override
    public PreviousAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_recycler_brown, parent, false);
        return new PreviousAdaptar.ViewHolder(view);
    }


    @Override
    public int getItemCount() {
        return orders.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recyclerViewBackground);
        }
    }
}
