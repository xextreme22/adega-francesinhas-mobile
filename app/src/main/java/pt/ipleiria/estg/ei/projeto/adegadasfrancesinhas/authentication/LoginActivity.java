package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hbb20.CountryCodePicker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class LoginActivity extends AppCompatActivity {

    private VideoView videoView;
    private Button buttonNext;
    private EditText editTextPhone;
    private TextView textViewSkip;
    private CountryCodePicker countryCodePicker;
    private ConstraintLayout constraintLayout;

    private FirebaseAuth mAuth;
    private FirebaseUser user;

    public static final Pattern VALID_PHONE_NUMBER_REGEX = Pattern.compile("^[/0-9]{1,9}$", Pattern.CASE_INSENSITIVE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        videoView = findViewById(R.id.videoView);
        buttonNext = findViewById(R.id.buttonNext);
        textViewSkip = findViewById(R.id.textViewSkip);
        editTextPhone = findViewById(R.id.editTextPhone);
        countryCodePicker = findViewById(R.id.countryCodePicker);
        constraintLayout = findViewById(R.id.constraintLayout);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = editTextPhone.getText().toString().trim();
                String phoneNumber = "+" + countryCodePicker.getSelectedCountryCode() + number;
                countryCodePicker.setFullNumber(phoneNumber);

                if(!validatePhoneNumber(number)){
                    editTextPhone.setError(getString(R.string.ERROR_INVALID_PHONE_NUMBER));
                    editTextPhone.requestFocus();
                    return;
                }

                Intent intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
                intent.putExtra("phoneNumber", phoneNumber);
                String position = String.valueOf(videoView.getCurrentPosition());
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        textViewSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogLoading dialogInfo = new DialogLoading();
                AlertDialog alertDialog = dialogInfo.loadingDialog(LoginActivity.this, getLayoutInflater());
                alertDialog.show();

                mAuth.signInAnonymously().addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        alertDialog.dismiss();
                        if (task.isSuccessful()) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            DialogInfo dialogInfo = new DialogInfo();
                            AlertDialog alertDialog = dialogInfo.infoDialog(LoginActivity.this, getLayoutInflater(), getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                            alertDialog.show();
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        user = mAuth.getCurrentUser();
        if(user != null){
            if(!user.isAnonymous()){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }

        startVideo();
    }

    private void startVideo(){
        videoView.setVideoPath("android.resource://"+getPackageName()+"/"+R.raw.bg_video);

        String position = getIntent().getStringExtra("position");
        if(position != null){
            videoView.seekTo(Integer.parseInt(position));
        }
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight(); //16:9 -> 1280x720
                float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
                float scaleX = videoRatio / screenRatio;
                if (scaleX >= 1f) {
                    videoView.setScaleX(scaleX);
                } else {
                    videoView.setScaleY(1f / scaleX);
                }
            }
        });
    }

    public static boolean validatePhoneNumber(String phoneStr) {
        Matcher matcher = VALID_PHONE_NUMBER_REGEX.matcher(phoneStr);
        return matcher.find();
    }

}
