package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.profile;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;

public class ProfileActivity extends AppCompatActivity {

    private Button buttonBack;

    private Button buttonFAQ;
    private Button buttonChangeName;
    private Button buttonLogout;
    private Button buttonLogin;

    private TextView textViewName;

    private CircleImageView imageViewProfilePicture;

    private Uri imageUri;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;

    public static final int IMAGE_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        userInfoInstance = UserInfoInstance.getInstance();

        buttonLogout = findViewById(R.id.buttonLogout);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonChangeName = findViewById(R.id.buttonChangeName);
        buttonFAQ = findViewById(R.id.buttonFAQ);
        buttonBack = findViewById(R.id.buttonBack);

        textViewName = findViewById(R.id.textViewName);

        imageViewProfilePicture = findViewById(R.id.imageViewProfilePicture);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                userInfoInstance.setUser(null);
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        buttonChangeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(ProfileActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_change_field, null);
                TextView textViewTitle =  mView.findViewById(R.id.textViewTitle);
                final EditText editTextName = mView.findViewById(R.id.editTextChange);
                Button buttonCancel = mView.findViewById(R.id.buttonCancel);
                Button buttonSave = mView.findViewById(R.id.buttonSave);

                editTextName.setText(userInfoInstance.getUser().getNome());
                textViewTitle.setText(getString(R.string.Change_name));

                mBuilder.setView(mView);
                final AlertDialog dialogChangeName = mBuilder.create();
                dialogChangeName.show();

                buttonSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String novoNome = editTextName.getText().toString().trim();

                        if (novoNome.length() > 64 || novoNome.length() < 1) {
                            editTextName.setError(getString(R.string.ERROR_INVALID_NAME));
                            editTextName.requestFocus();
                            return;
                        }

                        if(novoNome.equals(userInfoInstance.getUser().getNome())){
                            editTextName.setError(getString(R.string.ERROR_INVALID_NAME_SAME));
                            editTextName.requestFocus();
                            return;
                        }

                        db.collection(getString(R.string.DATABASE_USERS_COLLECTION)).document(user.getUid())
                                .update(getString(R.string.DATABASE_NAME_FIELD), novoNome).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialogChangeName.dismiss();
                                if(task.isSuccessful()){
                                    userInfoInstance.getUser().setNome(novoNome);
                                    textViewName.setText(novoNome);
                                }else{
                                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                                }
                            }
                        });
                    }
                });

                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogChangeName.dismiss();
                    }
                });
            }
        });


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        buttonFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog(getString(R.string.FAQ), getString(R.string.FAQ_info));
            }
        });

        imageViewProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        user = mAuth.getCurrentUser();
        if(user == null){
            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(userInfoInstance.getUser() != null){
                    updateUi();
                }
            }
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(ProfileActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void updateUi(){
        textViewName.setText(userInfoInstance.getUser().getNome());
        updatePhoto();
        buttonChangeName.setVisibility(View.VISIBLE);
        buttonLogout.setVisibility(View.VISIBLE);
        buttonLogin.setVisibility(View.GONE);
    }

    private void updatePhoto(){
        Uri photo = userInfoInstance.getFoto();
        if(photo != null){
            Picasso.get().load(userInfoInstance.getFoto()).into(imageViewProfilePicture);
        }else{
            imageViewProfilePicture.setImageResource(R.drawable.placeholder);
        }
    }

    public void openImage(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK){
            imageUri = data.getData();

            if(getFileExtension(imageUri).equals("jpg")){
                uploadImage();
            }
        }
    }

    public void uploadImage() {
        if(imageUri != null){
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("utilizadores/" + uid + "/avatar.jpg");
            storageRef.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()){
                        userInfoInstance.setFoto(imageUri);
                        updatePhoto();
                        imageUri = null;
                    }else{
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                    }
                }
            });
        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
}
