package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.restaurants;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Restaurant;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;

public class RestaurantDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;

    private String restaurantName;
    private Restaurant restaurant = null;

    private Button buttonBack;
    private ScrollView scrollView;
    private ImageView imageView;
    private EditText editTextScheduleDescription;
    private EditText editTextPhoneNumber;
    private TextView textViewRestaurantName;

    private GoogleMap mapApi;
    private SupportMapFragment mapFragment;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);

        restaurantName = getIntent().getStringExtra("restaurant");

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        storageRef = FirebaseStorage.getInstance().getReference();

        dialogLoading = DialogLoading.loadingDialog(RestaurantDetailsActivity.this, getLayoutInflater());

        buttonBack = findViewById(R.id.buttonBack);
        imageView = findViewById(R.id.imageView);
        editTextScheduleDescription = findViewById(R.id.editTextScheduleDescription);
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber);
        textViewRestaurantName = findViewById(R.id.textViewRestaurantName);
        scrollView = findViewById(R.id.scrollView);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(RestaurantDetailsActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(restaurant == null){
                dialogLoading.show();
                loadRestaurant();
            }
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(RestaurantDetailsActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void loadRestaurant(){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(restaurantName)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    if(task.getResult().exists()){
                        restaurant = new Restaurant(
                                task.getResult().getId(),
                                task.getResult().getString(getString(R.string.DATABASE_SCHEDULE_FIELD)),
                                task.getResult().get(getString(R.string.DATABASE_PHONE_FIELD)).toString(),
                                (Number) task.getResult().get(getString(R.string.DATABASE_LATITUDE_FIELD)),
                                (Number) task.getResult().get(getString(R.string.DATABASE_LONGITUDE_FIELD))
                        );

                        String ref = "restaurantes/" + restaurantName + ".jpg";
                        StorageReference itemRef = storageRef.child(ref);
                        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                restaurant.setUri(uri);
                                Picasso.get().load(uri).into(imageView);
                            }
                        });

                        mapFragment.getMapAsync(RestaurantDetailsActivity.this);
                        editTextScheduleDescription.setText(restaurant.getScheduleDescription());
                        textViewRestaurantName.setText(restaurant.getName());
                        editTextPhoneNumber.setText(restaurant.getTelephone());
                    }
                    dialogLoading.dismiss();
                }else{
                    dialogLoading.dismiss();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng pos = new LatLng(restaurant.getLat().doubleValue(), restaurant.getLon().doubleValue());
        googleMap.addMarker(new MarkerOptions().position(pos).title(getString(R.string.Restaurant)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));

        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
    }
}
