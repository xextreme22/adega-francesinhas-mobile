package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogYesNo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.OrderAdaptar;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.PromotionAdaptar;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.categories.CategoryActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.previous.PreviousActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.promotions.OrderPromotionsActivity;

public class ReservationOrderActivity extends AppCompatActivity {
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;

    private ReservationOrderInstance reservationOrderInstance = ReservationOrderInstance.getInstance();

    private ArrayList<Order> orderItems = new ArrayList<>();
    private RecyclerView recyclerViewOrders;
    private RecyclerView.Adapter adapterOrder;
    private RecyclerView.LayoutManager layoutManagerOrder;

    private ArrayList<Promotion> promotionItems = new ArrayList<>();
    private RecyclerView recyclerViewPromotions;
    private RecyclerView.Adapter adapterPromotion;
    private RecyclerView.LayoutManager layoutManagerPromotion;

    private Button buttonBack;
    private Button buttonPay;
    private Button buttonCancel;
    private TextView textViewTitle;
    private ImageView imageViewPromotion;
    private ImageView imageViewMenu;
    private ImageView imageViewPrevious;
    private EditText editTextComment;
    private ConstraintLayout constraintLayout;

    private AlertDialog dialogLoading;

    public final static int BRAINTREE_REQUEST_CODE = 1;
    public final static MediaType MEDIA_TYPE = MediaType.parse("application/json");

    private Number previousPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();
        buttonBack = findViewById(R.id.buttonBack);
        buttonPay = findViewById(R.id.buttonPay);
        imageViewPromotion = findViewById(R.id.imageViewPromotion);
        imageViewMenu = findViewById(R.id.imageViewMenu);
        imageViewPrevious = findViewById(R.id.imageViewAux);
        textViewTitle = findViewById(R.id.textViewTitle);
        editTextComment = findViewById(R.id.editTextComment);
        editTextComment.setEnabled(true);
        buttonCancel = findViewById(R.id.buttonCancel);
        dialogLoading = DialogLoading.loadingDialog(this, getLayoutInflater());
        constraintLayout = findViewById(R.id.constraintLayout);

        buttonCancel.setVisibility(View.VISIBLE);
        textViewTitle.setText(getString(R.string.Order));
        imageViewPrevious.setImageResource(R.drawable.icon_hourglass_white);
        buttonPay.setVisibility(View.GONE);
        editTextComment.setText(reservationOrderInstance.getReservation().getComentario());

        previousPoints = userInfoInstance.getUser().getPontos();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelReservation();
            }
        });

        editTextComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextComment.setFocusable(true);
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                if(inputMethodManager != null) {
                    inputMethodManager.toggleSoftInputFromWindow(constraintLayout.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });

        editTextComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                reservationOrderInstance.getReservation().setComentario(editTextComment.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                onBackPressed();
            }
        });

        imageViewPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                Intent intent = new Intent(ReservationOrderActivity.this, OrderPromotionsActivity.class);
                startActivity(intent);
            }
        });

        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                Intent intent = new Intent(ReservationOrderActivity.this, CategoryActivity.class);
                startActivity(intent);
            }
        });

        imageViewPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                Intent intent = new Intent(ReservationOrderActivity.this, PreviousActivity.class);
                startActivity(intent);
            }
        });

        buttonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if(reservationOrderInstance.getReservation().getPago()){
                    popupDialog(getString(R.string.ERROR), getString(R.string.Already_paid));
                    return;
                }

                if(orderItems.size() == 0){
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_EMPTY_ORDER));
                    return;
                }

                Calendar currentCalendar = Calendar.getInstance();
                Timestamp currentTimestamp = new Timestamp(new Date(currentCalendar.getTimeInMillis()));
                if(reservationOrderInstance.getReservation().getData().compareTo(currentTimestamp) <= 0){ //timeout if user takes to much time making order
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_EXPIRED_DATE_PAY));
                    return;
                }

                buttonPay.setEnabled(false);
                dialogLoading.show();
                startPaymentProcess();

            }
        });
    }

    private void hideKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null){
            if(inputMethodManager != null){
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(ReservationOrderActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(ReservationOrderActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    ReservationOrderActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    userInfoInstance.getUser().setPontos(previousPoints);
                    reservationOrderInstance.resetOrders();
                    reservationOrderInstance.resetPromotions();
                    reservationOrderInstance.resetTotal();
                    promotionItems = new ArrayList<>();
                    orderItems = new ArrayList<>();
                    buildRecyclerViews();
                    loadPromotions();
                    loadOrders();
                }
            }else{
                Intent intent = new Intent(ReservationOrderActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    public void buildRecyclerViews(){
        //promotions
        recyclerViewPromotions = findViewById(R.id.recyclerViewPromotions);
        recyclerViewPromotions.setHasFixedSize(false);
        layoutManagerPromotion = new LinearLayoutManager(this);
        adapterPromotion = new PromotionAdaptar(promotionItems, ReservationOrderActivity.this);
        recyclerViewPromotions.setLayoutManager(layoutManagerPromotion);
        recyclerViewPromotions.setAdapter(adapterPromotion);

        //orders
        recyclerViewOrders = findViewById(R.id.recyclerViewOrders);
        recyclerViewOrders.setHasFixedSize(false);
        layoutManagerOrder = new LinearLayoutManager(this);
        adapterOrder = new OrderAdaptar(orderItems, ReservationOrderActivity.this);
        recyclerViewOrders.setLayoutManager(layoutManagerOrder);
        recyclerViewOrders.setAdapter(adapterOrder);
    }

    @Override
    public void onBackPressed() {
        userInfoInstance.getUser().setPontos(previousPoints);
        reservationOrderInstance.resetAll();
        Intent intent = new Intent(ReservationOrderActivity.this, ReservationActivity.class);
        startActivity(intent);
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(ReservationOrderActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void loadPromotions(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).document(reservationOrderInstance.getReservationId())
                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            Promotion promotion = new Promotion(
                                    document.getString(getString(R.string.DATABASE_MENU_FIELD)),
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.getString(getString(R.string.DATABASE_PROMOTION_FIELD)),
                                    document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))
                            );
                            userInfoInstance.getUser().removePontos(promotion.getPreco().intValue() * promotion.getQuantidade());
                            reservationOrderInstance.addPromotion(promotion);
                            promotionItems.add(promotion);
                            adapterPromotion.notifyDataSetChanged();
                        }
                    }
                }else{
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadOrders(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).document(reservationOrderInstance.getReservationId())
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            Order order = new Order(
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.getString(getString(R.string.DATABASE_MENU_FIELD)),
                                    document.getString(getString(R.string.DATABASE_CATEGORY_FIELD)),
                                    document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD)));
                            reservationOrderInstance.addOrder(order);
                            orderItems.add(order);
                        }
                        adapterOrder.notifyDataSetChanged();
                        String buttonPayText = getString(R.string.Pay) + " : " + reservationOrderInstance.getTotal() + " " + getString(R.string.Currency);
                        buttonPay.setText(buttonPayText);
                        buttonPay.setVisibility(View.VISIBLE);
                    }
                }else{
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    private void startPaymentProcess(){
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(getString(R.string.CLOUD_FUNCTION_URL_GET_TOKEN))
                .addHeader("cache-control", "no-cache").addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json").build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialogLoading.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int responseCode = response.code();
                if(response.isSuccessful()){
                    dialogLoading.dismiss();
                    switch (responseCode){
                        case 200:
                            handleTokenRequest(response);
                            break;
                        default:
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_GENERATING_TOKEN));
                            break;
                    }
                }else{
                    ReservationOrderActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_GENERATING_TOKEN));
                            buttonPay.setEnabled(true);
                        }
                    });
                }
            }
        });
    }

    private void handleTokenRequest(Response response) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(response.body().byteStream()));
        StringBuilder total = new StringBuilder();
        for (String line; (line = r.readLine()) != null; ) {
            total.append(line).append('\n');
        }

        JSONObject myObject ;
        String clientToken;
        try {
            myObject = new JSONObject(total.toString());
            clientToken = myObject.get("clientToken").toString();
            DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken).disablePayPal();
            startActivityForResult(dropInRequest.getIntent(ReservationOrderActivity.this), BRAINTREE_REQUEST_CODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        dialogLoading.dismiss();
        if (requestCode == BRAINTREE_REQUEST_CODE && data != null) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                if(result.getPaymentMethodNonce() != null){
                    dialogLoading.show();
                    String nonce = result.getPaymentMethodNonce().getNonce();
                    sendServerRequest(nonce);
                }
            } else if (resultCode == RESULT_CANCELED) {
                buttonPay.setEnabled(true);
            } else{
                buttonPay.setEnabled(true);
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                popupDialog(getString(R.string.ERROR), error.getMessage());
            }
        }else{
            buttonPay.setEnabled(true);
        }
    }

    private void sendServerRequest(String nonce){
        OkHttpClient client = new OkHttpClient();
        JSONObject postData = new JSONObject();
        try {
            postData.put("nonce", nonce);
            postData.put("reservationId", reservationOrderInstance.getReservationId());
            postData.put("uid", user.getUid());
            postData.put("comment", reservationOrderInstance.getReservation().getComentario());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(MEDIA_TYPE, postData.toString());
        Request request = new Request.Builder()
                .url(getString(R.string.CLOUD_FUNCTION_URL_PROCESS_PAYMENT))
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialogLoading.dismiss();
                ReservationOrderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SERVER_ERROR));
                        buttonPay.setEnabled(true);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int responseCode = response.code();
                dialogLoading.dismiss();
                if(response.isSuccessful()){
                    switch (responseCode){
                        case 200:
                            ReservationOrderActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    userInfoInstance.getUser().addPontos(reservationOrderInstance.getTotal().intValue());
                                    Intent intent = new Intent(ReservationOrderActivity.this, DetailsReservationActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("id", reservationOrderInstance.getReservationId());
                                    reservationOrderInstance.resetAll();
                                    startActivity(intent);
                                }
                            });
                            break;
                        default:
                            ReservationOrderActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_UNEXPECTED_MESSAGE));
                                }
                            });
                            break;
                    }
                }else{
                    ReservationOrderActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_PAYMENT_CANCELLED_OPERATION));
                            buttonPay.setEnabled(true);
                        }
                    });
                }
            }
        });
    }

    private void cancelReservation(){
        DialogYesNo dialogYesNo = new DialogYesNo();
        AlertDialog alertDialogWarning = dialogYesNo.infoDialog(
                ReservationOrderActivity.this,
                getLayoutInflater(),
                getString(R.string.WARNING),
                getString(R.string.WARNING_CANCEL_RESERVATION),
                getString(R.string.Yes),
                getString(R.string.No));
        dialogYesNo.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialogLoading = DialogLoading.loadingDialog(ReservationOrderActivity.this, getLayoutInflater());
                alertDialogWarning.dismiss();
                dialogLoading.show();
                db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                        .document(reservationOrderInstance.getReservationId())
                        .update(getString(R.string.DATABASE_CANCELLED_FIELD), true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialogLoading.dismiss();
                        Intent intent = new Intent(ReservationOrderActivity.this, ReservationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialogLoading.dismiss();
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                });
            }
        });
        dialogYesNo.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogWarning.dismiss();
            }
        });
        alertDialogWarning.show();
    }
}
