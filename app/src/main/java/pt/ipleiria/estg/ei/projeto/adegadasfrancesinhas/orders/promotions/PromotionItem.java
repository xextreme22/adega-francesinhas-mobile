package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.promotions;

import android.net.Uri;

public class PromotionItem {
    private Uri uri;
    private String promotionId;
    private String category;
    private String restaurant;
    private String nameItem;
    private String pricePoints;


    public PromotionItem(Uri uri, String promotionId, String category, String restaurant, String nameItem, String pricePoints) {
        this.uri = uri;
        this.promotionId = promotionId;
        this.category = category;
        this.restaurant = restaurant;
        this.nameItem = nameItem;
        this.pricePoints = pricePoints;
    }

    public PromotionItem(String promotionId, String category, String restaurant, String nameItem, String pricePoints) {
        this.promotionId = promotionId;
        this.category = category;
        this.restaurant = restaurant;
        this.nameItem = nameItem;
        this.pricePoints = pricePoints;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getPricePoints() {
        return pricePoints;
    }

    public void setPricePoints(String pricePoints) {
        this.pricePoints = pricePoints;
    }
}
