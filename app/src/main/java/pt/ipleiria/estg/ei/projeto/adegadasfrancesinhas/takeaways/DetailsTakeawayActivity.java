package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogYesNo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.OrderAdaptar;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.PromotionAdaptar;

public class DetailsTakeawayActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private ArrayList<Order> orderItems;
    private RecyclerView recyclerViewOrders;
    private RecyclerView.Adapter adapterOrder;
    private RecyclerView.LayoutManager layoutManagerOrder;

    private ArrayList<Promotion> promotionItems;
    private RecyclerView recyclerViewPromotions;
    private RecyclerView.Adapter adapterPromotion;
    private RecyclerView.LayoutManager layoutManagerPromotion;

    private Button buttonBack;
    private Button buttonPay;
    private Button buttonCancel;
    private TextView textViewTitle;
    private TextView textViewComment;
    private TextView textViewOrderTitle;
    private ImageView imageViewOrderTitleBar;
    private ImageView imageViewPromotion;
    private ImageView imageViewMenu;
    private ImageView imageViewPhoneCall;
    private ImageView imageViewBottomSeparator;
    private ImageView imageViewCommentBackground;
    private EditText editTextComment;
    private ConstraintLayout constraintLayout;
    private AlertDialog dialogLoading;

    private String idTakeaway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        buttonBack = findViewById(R.id.buttonBack);
        buttonPay = findViewById(R.id.buttonPay);
        imageViewPromotion = findViewById(R.id.imageViewPromotion);
        imageViewMenu = findViewById(R.id.imageViewMenu);
        imageViewPhoneCall = findViewById(R.id.imageViewAux);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewComment = findViewById(R.id.textViewComment);
        textViewOrderTitle = findViewById(R.id.textViewOrderTitle);
        imageViewOrderTitleBar = findViewById(R.id.imageViewOrderTitleBar);
        editTextComment = findViewById(R.id.editTextComment);
        dialogLoading = DialogLoading.loadingDialog(this, getLayoutInflater());
        constraintLayout = findViewById(R.id.constraintLayout);
        buttonCancel = findViewById(R.id.buttonCancel);
        imageViewCommentBackground = findViewById(R.id.imageViewCommentBackground);

        idTakeaway = getIntent().getStringExtra("id");

        textViewTitle.setText(getString(R.string.Order_Details));
        imageViewPhoneCall.setImageResource(R.drawable.icon_phone_white);

        buttonCancel.setVisibility(View.VISIBLE);
        buttonPay.setVisibility(View.GONE);
        imageViewPromotion.setVisibility(View.GONE);
        imageViewMenu.setVisibility(View.GONE);
        imageViewPhoneCall.setVisibility(View.GONE);
        editTextComment.setVisibility(View.GONE);
        textViewComment.setVisibility(View.GONE);
        imageViewCommentBackground.setVisibility(View.GONE);
        imageViewOrderTitleBar.setVisibility(View.GONE);
        textViewOrderTitle.setVisibility(View.GONE);

        editTextComment.clearFocus();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTakeaway();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(DetailsTakeawayActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                dialogLoading.show();
                loadTakeaway();
                promotionItems = new ArrayList<>();
                orderItems = new ArrayList<>();
                buildRecyclerViews();
                loadOrders();
                loadPromotions();
            }else{
                Intent intent = new Intent(DetailsTakeawayActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DetailsTakeawayActivity.this, TakeawayActivity.class);
        startActivity(intent);
    }


    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(DetailsTakeawayActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerViews(){
        //promotions
        recyclerViewPromotions = findViewById(R.id.recyclerViewPromotions);
        recyclerViewPromotions.setHasFixedSize(false);
        layoutManagerPromotion = new LinearLayoutManager(this);
        adapterPromotion = new PromotionAdaptar(promotionItems, DetailsTakeawayActivity.this);
        recyclerViewPromotions.setLayoutManager(layoutManagerPromotion);
        recyclerViewPromotions.setAdapter(adapterPromotion);

        //orders
        recyclerViewOrders = findViewById(R.id.recyclerViewOrders);
        recyclerViewOrders.setHasFixedSize(false);
        layoutManagerOrder = new LinearLayoutManager(this);
        adapterOrder = new OrderAdaptar(orderItems, DetailsTakeawayActivity.this);
        recyclerViewOrders.setLayoutManager(layoutManagerOrder);
        recyclerViewOrders.setAdapter(adapterOrder);
    }

    public void loadTakeaway(){
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION)).document(idTakeaway).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot result = task.getResult();
                    String comentario = result.get(getString(R.string.DATABASE_COMMENT_FIELD)).toString();
                    if(!comentario.equals("")){
                        editTextComment.setText(comentario);
                        editTextComment.setVisibility(View.VISIBLE);
                        textViewComment.setVisibility(View.VISIBLE);
                        imageViewCommentBackground.setVisibility(View.VISIBLE);
                        imageViewOrderTitleBar.setVisibility(View.VISIBLE);
                        textViewOrderTitle.setVisibility(View.VISIBLE);
                    }
                    dialogLoading.dismiss();
                } else {
                    dialogLoading.dismiss();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadPromotions(){
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION)).document(idTakeaway)
                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            promotionItems.add(new Promotion(
                                    document.getString(getString(R.string.DATABASE_MENU_FIELD)),
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.getString(getString(R.string.DATABASE_PROMOTION_FIELD)),
                                    document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))
                                )
                            );
                            adapterPromotion.notifyDataSetChanged();
                        }
                    }
                }else{
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadOrders(){
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION)).document(idTakeaway)
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            orderItems.add(new Order(
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.getString(getString(R.string.DATABASE_MENU_FIELD)),
                                    document.getString(getString(R.string.DATABASE_CATEGORY_FIELD)),
                                    document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))
                                    )
                            );
                            adapterOrder.notifyDataSetChanged();
                        }
                    }
                }else{
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    private void cancelTakeaway(){
        DialogYesNo dialogYesNo = new DialogYesNo();
        AlertDialog alertDialogWarning = dialogYesNo.infoDialog(DetailsTakeawayActivity.this, getLayoutInflater(), getString(R.string.WARNING), getString(R.string.WARNING_CANCEL_TAKEAWAY), getString(R.string.Yes), getString(R.string.No));
        dialogYesNo.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialogLoading = DialogLoading.loadingDialog(DetailsTakeawayActivity.this, getLayoutInflater());
                alertDialogWarning.dismiss();
                dialogLoading.show();
                db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION)).document(idTakeaway).update(getString(R.string.DATABASE_CANCELLED_FIELD), true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialogLoading.dismiss();
                        Intent intent = new Intent(DetailsTakeawayActivity.this, TakeawayActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialogLoading.dismiss();
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                });
            }
        });
        dialogYesNo.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogWarning.dismiss();
            }
        });
        alertDialogWarning.show();
    }
}
