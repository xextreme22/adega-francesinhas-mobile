package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

public class Schedule {
    private String weekDay;
    private int openHour;
    private int closeHour;

    public Schedule(String weekDay, int openHour, int closeHour) {
        this.weekDay = weekDay;
        this.openHour = openHour;
        this.closeHour = closeHour;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public int getOpenHour() {
        return openHour;
    }

    public void setOpenHour(int openHour) {
        this.openHour = openHour;
    }

    public int getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(int closeHour) {
        this.closeHour = closeHour;
    }
}
