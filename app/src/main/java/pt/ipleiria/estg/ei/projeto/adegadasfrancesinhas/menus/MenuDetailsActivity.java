package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Allergen;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Menu;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;

public class MenuDetailsActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;

    private String restaurant;
    private String category;
    private String menuName;
    private Menu menu;

    private Button buttonBack;
    private ImageView imageView;
    private EditText editTextIngredients;
    private TextView textViewMenuName;

    private List<Allergen> allergens = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_details);

        restaurant = getIntent().getStringExtra("restaurant");
        category = getIntent().getStringExtra("category");
        menuName = getIntent().getStringExtra("menu");

        dialogLoading = DialogLoading.loadingDialog(MenuDetailsActivity.this, getLayoutInflater());

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        storageRef = FirebaseStorage.getInstance().getReference();

        buttonBack = findViewById(R.id.buttonBack);
        imageView = findViewById(R.id.imageView);
        editTextIngredients = findViewById(R.id.editTextIngredients);
        textViewMenuName = findViewById(R.id.textViewMenuName);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        adapter = new AllergenAdaptar(allergens);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(MenuDetailsActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            buildRecyclerView();
            dialogLoading.show();
            loadMenu();
            loadAllergens();
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(MenuDetailsActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void loadMenu(){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(restaurant)
                .collection(getString(R.string.DATABASE_CATEGORY_COLLECTION))
                .document(category)
                .collection(getString(R.string.DATABASE_ITEMS_COLLECTION))
                .document(menuName)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    if(task.getResult().exists()){
                        menu = new Menu(
                                task.getResult().getId(),
                                task.getResult().getString(getString(R.string.DATABASE_INGREDIENTS_FIELD)),
                                (Number) task.getResult().get(getString(R.string.DATABASE_PRICE_FIELD))
                        );

                        String ref = "restaurantes/menu/" + menuName + ".jpg";
                        StorageReference itemRef = storageRef.child(ref);
                        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                menu.setUri(uri);
                                Picasso.get().load(uri).into(imageView);
                            }
                        });

                        editTextIngredients.setText(menu.getIngredients());
                        textViewMenuName.setText(menu.getName());
                        dialogLoading.dismiss();
                    }
                }else{
                    dialogLoading.dismiss();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                }
            }
        });
    }

    private void loadAllergens(){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(restaurant)
                .collection(getString(R.string.DATABASE_CATEGORY_COLLECTION))
                .document(category)
                .collection(getString(R.string.DATABASE_ITEMS_COLLECTION))
                .document(menuName)
                .collection(getString(R.string.DATABASE_ALLERGENS_COLLECTION))
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String ref = "restaurantes/menu/alergenicos/" + document.getId() + ".png";
                        StorageReference itemRef = storageRef.child(ref);
                        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                allergens.add(new Allergen(uri, document.getString(getString(R.string.DATABASE_NAME_FIELD))));
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        });
    }
}
