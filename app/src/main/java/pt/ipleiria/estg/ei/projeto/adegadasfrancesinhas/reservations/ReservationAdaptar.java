package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Reservation;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogYesNo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class ReservationAdaptar extends RecyclerView.Adapter<ReservationAdaptar.ViewHolder> {
    private List<Reservation> reservations;
    private List<String> ids;
    private Context context;
    private LayoutInflater layoutInflater;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewTimes;
        public TextView textViewDate;
        public TextView textViewRestaurant;
        public TextView textViewNumberPeople;
        public TextView textViewStatus;
        public RelativeLayout relativeLayout;

        private String id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTimes = itemView.findViewById(R.id.textViewTimes);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewRestaurant = itemView.findViewById(R.id.textViewRestaurant);
            textViewNumberPeople = itemView.findViewById(R.id.textViewNumberPeople);
            textViewStatus = itemView.findViewById(R.id.textViewStatus);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public ReservationAdaptar(List<Reservation> reservations, List<String> ids, Context context, LayoutInflater layoutInflater) {
        this.reservations = reservations;
        this.ids = ids;
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @NonNull
    @Override
    public ReservationAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_reservation, parent, false);
        return new ReservationAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationAdaptar.ViewHolder holder, int position) {
        Reservation currentReservation = reservations.get(position);

        Timestamp timestamp = currentReservation.getData();
        Calendar cal = Calendar.getInstance(Locale.UK);
        cal.setTimeInMillis(timestamp.toDate().getTime());
        String date = DateFormat.format(context.getString(R.string.Date_format), cal).toString();
        String time = DateFormat.format(context.getString(R.string.Time_format), cal).toString();

        if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_WAITING_STATUS_VALUE))){
            holder.relativeLayout.setClickable(false);
        }

        holder.id = ids.get(position);
        holder.textViewTimes.setText(time);
        holder.textViewDate.setText(date);
        holder.textViewRestaurant.setText(currentReservation.getRestaurante());
        holder.textViewNumberPeople.setText(String.valueOf(currentReservation.getNumeroPessoas()));
        holder.textViewStatus.setText(currentReservation.getEstado());

        if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_ACCEPTED_STATUS_VALUE))){
            holder.textViewStatus.setTextColor(context.getResources().getColor(R.color.ReservationAccepted));
        }
        if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_REFUSED_STATUS_VALUE))){
            holder.textViewStatus.setTextColor(context.getResources().getColor(R.color.ReservationRefused));
        }
        if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_WAITING_STATUS_VALUE))){
            holder.textViewStatus.setTextColor(context.getResources().getColor(R.color.ReservationWaiting));
        }

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_ACCEPTED_STATUS_VALUE)) && !currentReservation.getPago()){
                    ReservationOrderInstance reservationOrderInstance = ReservationOrderInstance.getInstance();
                    reservationOrderInstance.resetAll();
                    reservationOrderInstance.setReservation(currentReservation);
                    reservationOrderInstance.setReservationId(holder.id);
                    Intent intent = new Intent(context, ReservationOrderActivity.class);
                    context.startActivity(intent);
                    return;
                }
                if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_ACCEPTED_STATUS_VALUE)) && currentReservation.getPago()){
                    Intent intent = new Intent(context, DetailsReservationActivity.class);
                    intent.putExtra("id", holder.id);
                    context.startActivity(intent);
                    return;
                }
                if(currentReservation.getEstado().equals(context.getString(R.string.DATABASE_RESERVATION_WAITING_STATUS_VALUE))){
                    cancelReservation(holder.id);
                }
            }
        });
    }

    private void cancelReservation(String id){
        DialogYesNo dialogYesNo = new DialogYesNo();
        AlertDialog alertDialogWarning = dialogYesNo.infoDialog(context, layoutInflater, context.getString(R.string.WARNING), context.getString(R.string.Cancel_reservation), context.getString(R.string.Yes), context.getString(R.string.No));
        dialogYesNo.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialogLoading = DialogLoading.loadingDialog(context, layoutInflater);
                alertDialogWarning.dismiss();
                dialogLoading.show();
                FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_RESERVATION_COLLECTION))
                        .document(id).update(context.getString(R.string.DATABASE_CANCELLED_FIELD), true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialogLoading.dismiss();
                        Intent intent = new Intent(context, ReservationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialogLoading.dismiss();
                        popupDialogError(context.getString(R.string.ERROR), context.getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                });
            }
        });
        dialogYesNo.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogWarning.dismiss();
            }
        });
        alertDialogWarning.show();
    }

    private void popupDialogError(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(context, layoutInflater, title, body);
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }
}
