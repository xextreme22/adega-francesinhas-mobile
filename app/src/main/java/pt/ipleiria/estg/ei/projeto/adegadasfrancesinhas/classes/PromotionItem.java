package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import android.net.Uri;
import android.widget.ImageView;

public class PromotionItem {
    private Uri uri;
    private String textItemName;
    private String textRestaurantName;
    private String textPointPrice;

    public PromotionItem(Uri uri, String textItemName, String textRestaurantName, String textPointPrice) {
        this.uri = uri;
        this.textItemName = textItemName;
        this.textRestaurantName = textRestaurantName;
        this.textPointPrice = textPointPrice;
    }

    public PromotionItem(String textItemName, String textRestaurantName, String textPointPrice) {
        this.textItemName = textItemName;
        this.textRestaurantName = textRestaurantName;
        this.textPointPrice = textPointPrice;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getTextItemName() {
        return textItemName;
    }

    public void setTextItemName(String textItemName) {
        this.textItemName = textItemName;
    }

    public String getTextRestaurantName() {
        return textRestaurantName;
    }

    public void setTextRestaurantName(String textRestaurantName) {
        this.textRestaurantName = textRestaurantName;
    }

    public String getTextPointPrice() {
        return textPointPrice;
    }

    public void setTextPointPrice(String textPointPrice) {
        this.textPointPrice = textPointPrice;
    }
}
