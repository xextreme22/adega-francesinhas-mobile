package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.previous;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations.ReservationOrderActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways.TakeawayOrderActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class PreviousOrderAdaptar extends RecyclerView.Adapter<PreviousOrderAdaptar.ViewHolder> {
    private List<Order> orderItems;
    private Context context;
    private LayoutInflater layoutInflater;
    private AlertDialog dialogLoading;
    private int aux;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewQuantity;
        public TextView textViewItemName;
        public RelativeLayout relativeLayoutItems;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
            textViewQuantity = itemView.findViewById(R.id.textViewQuantity);
            relativeLayoutItems = itemView.findViewById(R.id.relativeLayoutItems);
        }
    }

    public PreviousOrderAdaptar(List<Order> orderItems, Context context, LayoutInflater layoutInflater) {
        this.orderItems = orderItems;
        this.context = context;
        this.layoutInflater = layoutInflater;
        dialogLoading = DialogLoading.loadingDialog(context, layoutInflater);
    }

    @NonNull
    @Override
    public PreviousOrderAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_order_no_price, parent, false);
        return new PreviousOrderAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviousOrderAdaptar.ViewHolder holder, int position) {
        Order currentOrder = orderItems.get(position);

        holder.textViewItemName.setText(currentOrder.getMenu());
        String quantity = context.getString(R.string.Quantity_abbreviation) + " " + currentOrder.getQuantidade();
        holder.textViewQuantity.setText(quantity);

        holder.relativeLayoutItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.relativeLayoutItems.setEnabled(false);
                AlertDialog dialogLoading = DialogLoading.loadingDialog(context, layoutInflater);
                dialogLoading.show();
                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    storeOrdersTakeaway(orderItems);
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    storeOrdersReservation(orderItems);
                }
            }
        });
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(context, layoutInflater, title, body);
        alertDialog.show();
    }

    private void storeOrdersReservation(List<Order> orderItems){
        FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_RESERVATION_COLLECTION))
                .document(ReservationOrderInstance.getInstance().getReservationId())
                .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(context.getString(R.string.DATABASE_QUANTITY_FIELD));
                    Order order = new Order(
                            quantidade.intValue(),
                            doc.getString(context.getString(R.string.DATABASE_MENU_FIELD)),
                            doc.getString(context.getString(R.string.DATABASE_CATEGORY_FIELD)),
                            doc.getString(context.getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(context.getString(R.string.DATABASE_PRICE_FIELD))
                    );

                    int pos = getPositionOrders(orderItems, order);
                    if(pos != -1){ //se a order ja ta stored update quantity
                        Number newQuantity = orderItems.get(pos).getQuantidade() + quantidade.intValue();
                        orderItems.remove(pos);
                        FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_RESERVATION_COLLECTION))
                                .document(ReservationOrderInstance.getInstance().getReservationId())
                                .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).update(context.getString(R.string.DATABASE_QUANTITY_FIELD), newQuantity);
                    }
                }
                addNewOrdersReservation(orderItems);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(context.getString(R.string.ERROR), e.getMessage());
            }
        });
    }

    private void addNewOrdersReservation(List<Order> orderItems){
        aux = 0;
        dialogLoading.show();
        if(orderItems.size() == 0){
            dialogLoading.dismiss();
            Intent intent = new Intent(context, ReservationOrderActivity.class);
            context.startActivity(intent);
            return;
        }

        for (Order order : orderItems){
            FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_RESERVATION_COLLECTION))
                    .document(ReservationOrderInstance.getInstance().getReservationId())
                    .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).add(order).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    aux++;
                    if(aux == orderItems.size()){
                        dialogLoading.dismiss();
                        Intent intent = new Intent(context, ReservationOrderActivity.class);
                        context.startActivity(intent);
                    }
                }
            });
        }
    }

    private void storeOrdersTakeaway(List<Order> orderItems){
        FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(context.getString(R.string.DATABASE_QUANTITY_FIELD));
                    Order order = new Order(
                            quantidade.intValue(),
                            doc.getString(context.getString(R.string.DATABASE_MENU_FIELD)),
                            doc.getString(context.getString(R.string.DATABASE_CATEGORY_FIELD)),
                            doc.getString(context.getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(context.getString(R.string.DATABASE_PRICE_FIELD))
                    );

                    int pos = getPositionOrders(orderItems, order);
                    if(pos != -1){ //se a order ja ta stored update quantity
                        Number newQuantity = orderItems.get(pos).getQuantidade() + quantidade.intValue();
                        orderItems.remove(pos);
                        FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                                .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).document(doc.getId()).update(context.getString(R.string.DATABASE_QUANTITY_FIELD), newQuantity);
                    }
                }
                addNewOrdersTakeaway(orderItems);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(context.getString(R.string.ERROR), e.getMessage());
            }
        });
    }

    private void addNewOrdersTakeaway(List<Order> orderItems){
        aux = 0;
        dialogLoading.show();
        if(orderItems.size() == 0){
            dialogLoading.dismiss();
            Intent intent = new Intent(context, TakeawayOrderActivity.class);
            context.startActivity(intent);
            return;
        }

        for (Order order : orderItems){
            FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                    .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                    .collection(context.getString(R.string.DATABASE_ORDER_COLLECTION)).add(order).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    aux++;
                    if(aux == orderItems.size()){
                        dialogLoading.dismiss();
                        Intent intent = new Intent(context, TakeawayOrderActivity.class);
                        context.startActivity(intent);
                    }
                }
            });
        }
    }

    private int getPositionOrders(List<Order> orders, Order order){
        int i = 0;
        for (Order aux: orders) {
            if(aux.getRestaurante().equals(order.getRestaurante())) {
                if (aux.getMenu().equals(order.getMenu())) {
                    if (aux.getCategoria().equals(order.getCategoria())) {
                        return i;
                    }
                }
            }
            i++;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }
}
