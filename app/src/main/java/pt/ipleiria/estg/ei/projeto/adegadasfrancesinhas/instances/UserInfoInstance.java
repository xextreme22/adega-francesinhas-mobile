package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances;

import android.net.Uri;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.User;

public class UserInfoInstance {

    private static UserInfoInstance INSTANCE = null;

    private User user;
    private Uri foto;

    private UserInfoInstance() {
        user = null;
        foto = null;
    }

    public static UserInfoInstance getInstance() {
        if(INSTANCE == null){
            INSTANCE = new UserInfoInstance();
        }
        return INSTANCE;
    }

    public static void resetInstance(){
        INSTANCE = null;
    }

    public Uri getFoto() {
        return foto;
    }

    public void setFoto(Uri foto) {
        this.foto = foto;
    }

    public User getUser() { //can return null
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
