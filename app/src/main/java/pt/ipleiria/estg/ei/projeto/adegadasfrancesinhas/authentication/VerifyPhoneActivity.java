package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.concurrent.TimeUnit;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.User;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class VerifyPhoneActivity extends AppCompatActivity {

    private VideoView videoView;

    private Button buttonNext;
    private Button buttonBack;

    private EditText editTextCode;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseFirestore db;
    private StorageReference storageRef;

    private String verificationId;
    private AlertDialog dialogLoading;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        String phoneNumber = getIntent().getStringExtra("phoneNumber");

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        videoView = findViewById(R.id.videoView);

        buttonNext = findViewById(R.id.buttonNext);
        buttonBack = findViewById(R.id.buttonBack);

        editTextCode = findViewById(R.id.editTextCode);

        mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationId = s;
                buttonNext.setVisibility(View.VISIBLE);
            }

            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                String code = phoneAuthCredential.getSmsCode();
                if(code != null){
                    editTextCode.setText(code);
                    verifyCode(code);
                }
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
            }
        };

        sendVerificationCode(phoneNumber);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerifyPhoneActivity.this, LoginActivity.class);
                //New activity and closes all other past activitis
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                String position = String.valueOf(videoView.getCurrentPosition());
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();

                if(code.isEmpty() || code.length()<6){
                    editTextCode.setError(getString(R.string.ERROR_INVALID_CODE));
                    editTextCode.requestFocus();
                    return;
                }

                verifyCode(code);
            }
        });
    }

    private void verifyCode(String code){
        dialogLoading = DialogLoading.loadingDialog(VerifyPhoneActivity.this, getLayoutInflater());
        dialogLoading.show();

        //todo used to be able to login on android emulator, remove when deploy
        if(verificationId == null){
            verificationId="123456";
        }


        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    user = mAuth.getCurrentUser();

                    String ref = "utilizadores/" + user.getUid() + "/avatar.jpg";
                    StorageReference itemRef = storageRef.child(ref);
                    itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            UserInfoInstance.getInstance().setFoto(uri);
                        }
                    });

                    DocumentReference userInfo = db.collection(getString(R.string.DATABASE_USERS_COLLECTION)).document(user.getUid());
                    userInfo.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            dialogLoading.dismiss();
                            if(task.isSuccessful()){
                                DocumentSnapshot document = task.getResult();
                                if(document.exists()){
                                    FirebaseMessaging.getInstance().subscribeToTopic(user.getUid());

                                    UserInfoInstance.getInstance().setUser(new User(
                                            document.getString(getString(R.string.DATABASE_NAME_FIELD)),
                                            document.getString(getString(R.string.DATABASE_PHONE_NUMBER_FIELD)),
                                            Integer.parseInt(document.get(getString(R.string.DATABASE_POINTS_FIELD)).toString()),
                                            document.getString(getString(R.string.DATABASE_ROLE_FIELD)),
                                            document.getBoolean(getString(R.string.DATABASE_DISABLED_FIELD))
                                    ));

                                    Intent intent = new Intent(VerifyPhoneActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }else{
                                    Intent intent = new Intent(VerifyPhoneActivity.this, RegisterActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    String position = String.valueOf(videoView.getCurrentPosition());
                                    intent.putExtra("position", position);
                                    startActivity(intent);
                                }
                            }else{
                                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                            }
                        }
                    });
                }else{
                    dialogLoading.dismiss();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    private void sendVerificationCode(String phoneNumber){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }

    @Override
    protected void onStart() {
        super.onStart();

        user = mAuth.getCurrentUser();
        if(user != null){
            if(!user.isAnonymous()){
                finish();
            }
        }

        startVideo();
    }

    private void startVideo(){
        videoView.setVideoPath("android.resource://"+getPackageName()+"/"+R.raw.bg_video);
        String position = getIntent().getStringExtra("position");
        if(position != null){
            videoView.seekTo(Integer.parseInt(position));
        }
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight(); //16:9 -> 1280x720
                float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
                float scaleX = videoRatio / screenRatio;
                if (scaleX >= 1f) {
                    videoView.setScaleX(scaleX);
                } else {
                    videoView.setScaleY(1f / scaleX);
                }
            }
        });
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(VerifyPhoneActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }
}
