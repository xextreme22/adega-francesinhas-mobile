package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import com.google.firebase.Timestamp;

public class Takeaway {
    private Timestamp data;
    private String utilizador;
    private String restaurante;
    private Boolean cancelado;
    private String comentario;
    private Boolean pago;
    private String nome;
    private String telemovel;
    private String estado;

    public Takeaway(Timestamp data, String utilizador, String restaurante, String nome, String telemovel) {
        this.data = data;
        this.utilizador = utilizador;
        this.restaurante = restaurante;
        this.cancelado = false;
        this.comentario = "";
        this.pago = false;
        this.nome = nome;
        this.telemovel = telemovel;
        this.estado = "aceite";
    }

    public Takeaway(Timestamp data, String utilizador, String restaurante, Boolean cancelado, String comentario, Boolean pago, String nome, String telemovel, String estado) {
        this.data = data;
        this.utilizador = utilizador;
        this.restaurante = restaurante;
        this.cancelado = cancelado;
        this.comentario = comentario;
        this.pago = pago;
        this.nome = nome;
        this.telemovel = telemovel;
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelemovel() {
        return telemovel;
    }

    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(String restaurante) {
        this.restaurante = restaurante;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Boolean getPago() {
        return pago;
    }

    public void setPago(Boolean pago) {
        this.pago = pago;
    }
}
