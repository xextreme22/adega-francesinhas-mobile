package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

public class TakeawayItem {
    private String id;
    private String textViewTimes;
    private String textViewDate;
    private String textViewRestaurant;
    private Boolean pago;

    public TakeawayItem(String id, String textViewTimes, String textViewDate, String textViewRestaurant, Boolean pago) {
        this.id = id;
        this.textViewTimes = textViewTimes;
        this.textViewDate = textViewDate;
        this.textViewRestaurant = textViewRestaurant;
        this.pago = pago;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTextViewTimes() {
        return textViewTimes;
    }

    public void setTextViewTimes(String textViewTimes) {
        this.textViewTimes = textViewTimes;
    }

    public String getTextViewDate() {
        return textViewDate;
    }

    public void setTextViewDate(String textViewDate) {
        this.textViewDate = textViewDate;
    }

    public String getTextViewRestaurant() {
        return textViewRestaurant;
    }

    public void setTextViewRestaurant(String textViewRestaurant) {
        this.textViewRestaurant = textViewRestaurant;
    }

    public Boolean getPago() {
        return pago;
    }

    public void setPago(Boolean pago) {
        this.pago = pago;
    }
}
