package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.TakeawayItem;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class TakeawayActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private Button buttonBack;
    private Button buttonOrder;
    private TextView textViewTitle;
    private TextView textViewNoItems;

    private List<TakeawayItem> takeawaysList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private Boolean onePopupAux;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_recycler_button);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        buildRecyclerView();

        buttonBack = findViewById(R.id.buttonBack);
        buttonOrder = findViewById(R.id.buttonBottom);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        textViewNoItems.setText(getString(R.string.INFO_NO_TAKEAWAYS));

        dialogLoading = DialogLoading.loadingDialog(TakeawayActivity.this, getLayoutInflater());

        textViewTitle.setText(getString(R.string.Takeaways));
        buttonOrder.setText(getString(R.string.Order));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TakeawayActivity.this, CreateTakeawayActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TakeawayActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new TakeawayAdaptar(takeawaysList, TakeawayActivity.this, getLayoutInflater());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(TakeawayActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(TakeawayActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    if(takeawaysList.size() == 0){
                        dialogLoading.show();
                        loadTakeaways();
                    }
                }
            }else{
                Intent intent = new Intent(TakeawayActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void popupDialogError(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(TakeawayActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void loadTakeaways(){
        onePopupAux = true;
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.HOUR_OF_DAY, -1);
        Timestamp currentTimestamp = new Timestamp(new Date(currentCalendar.getTimeInMillis()));
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_CANCELLED_FIELD), false)
                .whereEqualTo(getString(R.string.DATABASE_USER_FIELD), user.getUid())
                .whereGreaterThanOrEqualTo(getString(R.string.DATABASE_DATE_FIELD), currentTimestamp)
                .orderBy(getString(R.string.DATABASE_DATE_FIELD), Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else {
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {

                                    if (!document.getBoolean(getString(R.string.DATABASE_PAYED_FIELD)) && onePopupAux) {
                                        popupDialogError(getString(R.string.WARNING), getString(R.string.ERROR_INCOMPLETE_TAKEAWAY));
                                        onePopupAux = false;
                                    }

                                    String restaurante = document.get(getString(R.string.DATABASE_RESTAURANT_FIELD)).toString();
                                    Timestamp timestamp = (Timestamp) document.get(getString(R.string.DATABASE_DATE_FIELD));
                                    Calendar cal = Calendar.getInstance(Locale.UK);
                                    cal.setTimeInMillis(timestamp.toDate().getTime());
                                    String date = DateFormat.format(getString(R.string.Date_format), cal).toString();
                                    String time = DateFormat.format(getString(R.string.Time_format), cal).toString();
                                    takeawaysList.add(new TakeawayItem(document.getId(), time, date, restaurante, document.getBoolean(getString(R.string.DATABASE_PAYED_FIELD))));
                                }
                                adapter.notifyDataSetChanged();
                            }
                                dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }
}
