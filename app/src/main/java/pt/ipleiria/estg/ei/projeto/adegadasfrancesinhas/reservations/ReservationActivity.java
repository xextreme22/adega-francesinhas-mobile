package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Reservation;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class ReservationActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private List<Reservation> reservations = new ArrayList<>();
    private List<String> ids = new ArrayList<>();

    private Button buttonBack;
    private Button buttonReserve;
    private TextView textViewTitle;
    private TextView textViewNoItems;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_recycler_button);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        buildRecyclerView();

        buttonBack = findViewById(R.id.buttonBack);
        buttonReserve = findViewById(R.id.buttonBottom);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        textViewNoItems.setText(getString(R.string.INFO_NO_RESERVATIONS));

        dialogLoading = DialogLoading.loadingDialog(ReservationActivity.this, getLayoutInflater());

        textViewTitle.setText(getString(R.string.Reservations));
        buttonReserve.setText(getString(R.string.Reserve));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReservationActivity.this, CreateReservationActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ReservationActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new ReservationAdaptar(reservations, ids, ReservationActivity.this, getLayoutInflater());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(ReservationActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(ReservationActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    if(reservations.size() == 0) {
                        dialogLoading.show();
                        loadReservations();
                    }
                }
            }else{
                Intent intent = new Intent(ReservationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }


    private void popupDialogError(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(ReservationActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void loadReservations(){
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.HOUR_OF_DAY, -1);
        Timestamp currentTimestamp = new Timestamp(new Date(currentCalendar.getTimeInMillis()));
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_CANCELLED_FIELD), false)
                .whereEqualTo(getString(R.string.DATABASE_USER_FIELD), user.getUid())
                .whereGreaterThanOrEqualTo(getString(R.string.DATABASE_DATE_FIELD), currentTimestamp)
                .orderBy(getString(R.string.DATABASE_DATE_FIELD), Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else {
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    reservations.add(new Reservation(
                                            (Timestamp) document.get(getString(R.string.DATABASE_DATE_FIELD)),
                                            document.getString(getString(R.string.DATABASE_USER_FIELD)),
                                            document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                            Integer.valueOf(document.get(getString(R.string.DATABASE_NUMBER_PEOPLE_FIELD)).toString()),
                                            document.getString(getString(R.string.DATABASE_STATUS_FIELD)),
                                            (Boolean) document.get(getString(R.string.DATABASE_CANCELLED_FIELD)),
                                            document.getString(getString(R.string.DATABASE_COMMENT_FIELD)),
                                            (Boolean) document.get(getString(R.string.DATABASE_PAYED_FIELD)),
                                            document.getString(getString(R.string.DATABASE_NAME_FIELD)),
                                            document.getString(getString(R.string.DATABASE_PHONE_NUMBER_FIELD))
                                    ));
                                    ids.add(document.getId());
                                }
                                adapter.notifyDataSetChanged();
                            }
                                dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }
}
