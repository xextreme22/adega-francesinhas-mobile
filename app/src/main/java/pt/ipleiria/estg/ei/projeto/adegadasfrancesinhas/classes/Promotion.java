package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

public class Promotion {
    private String menu;
    private int quantidade;
    private String promocao;
    private String restaurante;
    private Number preco;

    public Promotion(String menu, int quantidade, String promocao, String restaurante, Number preco) {
        this.menu = menu;
        this.quantidade = quantidade;
        this.promocao = promocao;
        this.restaurante = restaurante;
        this.preco = preco;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getPromocao() {
        return promocao;
    }

    public void setPromocao(String promocao) {
        this.promocao = promocao;
    }

    public String getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(String restaurante) {
        this.restaurante = restaurante;
    }

    public Number getPreco() {
        return preco;
    }

    public void setPreco(Number preco) {
        this.preco = preco;
    }

    public void decrementQuantity(){
        this.quantidade--;
    }

    public void incrementQuantity(){
        this.quantidade++;
    }

    public Promotion clone(){
        return new Promotion(menu, quantidade, promocao, restaurante, preco.doubleValue());
    }
}
