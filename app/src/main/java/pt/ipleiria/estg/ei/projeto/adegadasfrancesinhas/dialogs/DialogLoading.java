package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class DialogLoading {
    public static AlertDialog loadingDialog(Context context, LayoutInflater layoutInflater){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View mView = layoutInflater.inflate(R.layout.dialog_progress, null);
        mBuilder.setView(mView);
        return mBuilder.create();
    }
}
