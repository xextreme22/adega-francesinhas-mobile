package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScheduleTimes {
    private List<Timepoint> listTimepoint;

    //quando temos 2 datas no mesmo dia dia 1 hora a outra hora
    public ScheduleTimes() {
        listTimepoint = new ArrayList<>();
    }

    public static Timepoint timepoint(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        Timepoint timepoint = new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        return timepoint;
    }

    public void addTimepointsBetween(Calendar openCalendar, Calendar closeCalendar){
        if((openCalendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(openCalendar);
        }
        if((closeCalendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(closeCalendar);
        }

        while(openCalendar.compareTo(closeCalendar) != 1){
            Timepoint timepoint = new Timepoint(openCalendar.get(Calendar.HOUR_OF_DAY), openCalendar.get(Calendar.MINUTE));
            listTimepoint.add(timepoint);
            openCalendar.add(Calendar.MINUTE, 5);
        }
    }

    public void addTimepointsUntilEndDay(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        Calendar auxCalendar = (Calendar) calendar.clone();
        auxCalendar.set(Calendar.HOUR_OF_DAY, 23);
        auxCalendar.set(Calendar.MINUTE, 55);
        while(calendar.compareTo(auxCalendar) != 1){
            Timepoint timepoint = new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            listTimepoint.add(timepoint);
            calendar.add(Calendar.MINUTE, 5);
        }
    }

    public void addTimepointsFromStartDay(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        Calendar auxCalendar = (Calendar) calendar.clone();
        auxCalendar.set(Calendar.HOUR_OF_DAY, 0);
        auxCalendar.set(Calendar.MINUTE, 0);
        while(auxCalendar.compareTo(calendar) != 1){
            Timepoint timepoint = new Timepoint(auxCalendar.get(Calendar.HOUR_OF_DAY), auxCalendar.get(Calendar.MINUTE));
            listTimepoint.add(timepoint);
            auxCalendar.add(Calendar.MINUTE, 5);
        }
    }

    public void removeTimepointsUntilEndDay(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        Calendar auxCalendar = (Calendar) calendar.clone();
        auxCalendar.set(Calendar.HOUR_OF_DAY, 23);
        auxCalendar.set(Calendar.MINUTE, 55);
        while(calendar.compareTo(auxCalendar) != 1){
            Timepoint timepoint = new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

            for (int i = 0; i < listTimepoint.size(); i++) {
                Timepoint time = listTimepoint.get(i);
                if(timepoint.compareTo(time) == 0){
                    listTimepoint.remove(time);
                }
            }

            calendar.add(Calendar.MINUTE, 5);
        }
    }

    public void removeTimepointsBetween(Calendar openCalendar, Calendar closeCalendar){
        if((openCalendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(openCalendar);
        }
        if((closeCalendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(closeCalendar);
        }

        while(openCalendar.compareTo(closeCalendar) != 1){
            Timepoint timepoint = new Timepoint(openCalendar.get(Calendar.HOUR_OF_DAY), openCalendar.get(Calendar.MINUTE));

            for (int i = 0; i < listTimepoint.size(); i++) {
                Timepoint time = listTimepoint.get(i);
                if(timepoint.compareTo(time) == 0){
                    listTimepoint.remove(time);
                }
            }

            openCalendar.add(Calendar.MINUTE, 5);
        }
    }

    public void removeTimepointsFromStartDay(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        Calendar auxCalendar = (Calendar) calendar.clone();
        auxCalendar.set(Calendar.HOUR_OF_DAY, 0);
        auxCalendar.set(Calendar.MINUTE, 0);
        while(auxCalendar.compareTo(calendar) != 1){
            Timepoint timepoint = new Timepoint(auxCalendar.get(Calendar.HOUR_OF_DAY), auxCalendar.get(Calendar.MINUTE));

            for (int i = 0; i < listTimepoint.size(); i++) {
                Timepoint time = listTimepoint.get(i);
                if(timepoint.compareTo(time) == 0){
                    listTimepoint.remove(time);
                }
            }
            auxCalendar.add(Calendar.MINUTE, 5);
        }
    }

    public void addTimepointsCompleteDay(){
        Timepoint endTimepoint = new Timepoint(23, 55);
        Timepoint iteratorTimepoint = new Timepoint(0, 0);
        do{
            listTimepoint.add(new Timepoint(iteratorTimepoint.getHour(), iteratorTimepoint.getMinute()));
            iteratorTimepoint.add(Timepoint.TYPE.MINUTE, 5);
        }while(iteratorTimepoint.getHour() != endTimepoint.getHour() || iteratorTimepoint.getMinute() != endTimepoint.getMinute());
    }

    public List<Timepoint> getListTimepoint() {
        return listTimepoint;
    }

    private static void normalizeDate(Calendar calendar){
        int minute = calendar.get(Calendar.MINUTE);
        switch (minute%5){
            case 1:
                calendar.add(Calendar.MINUTE, 4);
                break;
            case 2:
                calendar.add(Calendar.MINUTE, 3);
                break;
            case 3:
                calendar.add(Calendar.MINUTE, 2);
                break;
            case 4:
                calendar.add(Calendar.MINUTE, 1);
                break;
        }
    }

    public List<Timepoint> timepointsNotBetween(Timepoint startTime, Timepoint endTime){
        List<Timepoint> timepoints = new ArrayList<>();
        for (Timepoint timepoint :listTimepoint){
            if(timepoint.compareTo(startTime) < 0){
                timepoints.add(timepoint);
            }
            if(timepoint.compareTo(endTime) > 0){
                timepoints.add(timepoint);
            }
        }
        return timepoints;
    }

    public static Timepoint convertToTimePoint(Calendar calendar){
        if((calendar.get(Calendar.MINUTE) % 5) != 0){
            normalizeDate(calendar);
        }

        return new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }
}
