package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Takeaway;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.TakeawayItem;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class TakeawayAdaptar extends RecyclerView.Adapter<TakeawayAdaptar.ViewHolder> {
    private List<TakeawayItem> takeawayItems;

    private Context context;
    private LayoutInflater layoutInflater;
    private AlertDialog dialogLoading;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewTimes;
        public TextView textViewDate;
        public TextView textViewRestaurant;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTimes = itemView.findViewById(R.id.textViewTimes);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewRestaurant = itemView.findViewById(R.id.textViewRestaurant);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public TakeawayAdaptar(List<TakeawayItem> takeawayItems, Context context, LayoutInflater layoutInflater) {
        this.takeawayItems = takeawayItems;
        this.context = context;
        this.layoutInflater = layoutInflater;
        this.dialogLoading = DialogLoading.loadingDialog(context, layoutInflater);
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(context, layoutInflater, title, body);
        alertDialog.show();
    }

    @NonNull
    @Override
    public TakeawayAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_takeaway, parent, false);
        return new TakeawayAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TakeawayAdaptar.ViewHolder holder, int position) {
        TakeawayItem currentItem = takeawayItems.get(position);

        holder.textViewTimes.setText(currentItem.getTextViewTimes());
        holder.textViewDate.setText(currentItem.getTextViewDate());
        holder.textViewRestaurant.setText(currentItem.getTextViewRestaurant());

        if(!currentItem.getPago()){
            holder.relativeLayout.setBackgroundResource(R.drawable.shadow_red);
        }else{
            color++;
            switch (color % 3){
                case 0:
                    holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                    break;
                case 1:
                    holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                    break;
                case 2:
                    holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                    break;
            }
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentItem.getPago()){
                    Intent intent = new Intent(context, DetailsTakeawayActivity.class);
                    intent.putExtra("id", currentItem.getId());
                    context.startActivity(intent);
                }else{
                    dialogLoading.show();
                    FirebaseFirestore.getInstance().collection(context.getString(R.string.DATABASE_TAKEAWAY_COLLECTION)).document(currentItem.getId()).get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if(task.isSuccessful()){
                                        DocumentSnapshot result = task.getResult();
                                        TakeawayOrderInstance takeawayOrderInstance = TakeawayOrderInstance.getInstance();
                                        takeawayOrderInstance.resetAll();
                                        takeawayOrderInstance.setTakeaway(
                                                new Takeaway(
                                                        result.getTimestamp(context.getString(R.string.DATABASE_DATE_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_USER_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                                        result.getBoolean(context.getString(R.string.DATABASE_CANCELLED_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_COMMENT_FIELD)),
                                                        result.getBoolean(context.getString(R.string.DATABASE_PAYED_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_NAME_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_PHONE_NUMBER_FIELD)),
                                                        result.getString(context.getString(R.string.DATABASE_STATUS_FIELD))
                                                )
                                        );
                                        takeawayOrderInstance.setTakeawayId(task.getResult().getId());
                                        dialogLoading.dismiss();
                                        Intent intent = new Intent(context, TakeawayOrderActivity.class);
                                        context.startActivity(intent);
                                    }else{
                                        dialogLoading.dismiss();
                                        popupDialog(context.getString(R.string.ERROR), context.getString(R.string.ERROR_DEFAULT_MESSAGE));
                                    }
                                }
                            });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return takeawayItems.size();
    }
}
