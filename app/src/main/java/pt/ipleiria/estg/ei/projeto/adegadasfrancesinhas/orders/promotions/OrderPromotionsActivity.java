package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.promotions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.LinkedList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class OrderPromotionsActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;
    private UserInfoInstance userInfoInstance;

    private LinkedList<PromotionItem> promotionsList = new LinkedList<>();

    private TextView textViewPoints;
    private TextView textViewNoItems;
    private Button buttonBack;
    private Button buttonFinalize;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String restaurant;
    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_promotions);

        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            restaurant = TakeawayOrderInstance.getInstance().getTakeaway().getRestaurante();
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            restaurant = ReservationOrderInstance.getInstance().getReservation().getRestaurante();
        }

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        buttonBack = findViewById(R.id.buttonBack);
        buttonFinalize = findViewById(R.id.buttonFinalize);
        textViewPoints = findViewById(R.id.textViewPoints);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        dialogLoading = DialogLoading.loadingDialog(this, getLayoutInflater());

        buildRecyclerView(); //has to be done after textViewPoints since we pass it to the adaptar

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonFinalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFinalize.setEnabled(false);
                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    storePromotionsTakeaway();
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    storePromotionsReservation();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(OrderPromotionsActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PromotionsAdaptar(promotionsList, textViewPoints, OrderPromotionsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(OrderPromotionsActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(OrderPromotionsActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    textViewPoints.setText(userInfoInstance.getUser().getPontos().toString());
                    if(promotionsList.size() == 0){
                        dialogLoading.show();
                        loadList();
                    }
                }
            }else{
                Intent intent = new Intent(OrderPromotionsActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void loadList(){
        db.collection(getString(R.string.DATABASE_PROMOTION_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .whereEqualTo(getString(R.string.DATABASE_RESTAURANT_FIELD), restaurant)
                .orderBy(getString(R.string.DATABASE_POINTS_FIELD))
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else {
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    document.getData();

                                    if (!(Boolean) document.get(getString(R.string.DATABASE_DISABLED_FIELD))) {
                                        final String itemName = document.get(getString(R.string.DATABASE_ITEM_FIELD)).toString();
                                        final String points = document.get(getString(R.string.DATABASE_POINTS_FIELD)).toString();
                                        final String restaurant = document.get(getString(R.string.DATABASE_RESTAURANT_FIELD)).toString();
                                        final String category = document.get(getString(R.string.DATABASE_CATEGORY_FIELD)).toString();
                                        final String promotionId = document.getId();

                                        PromotionItem promotion = new PromotionItem(promotionId, category, restaurant, itemName, points);
                                        promotionsList.add(promotion);

                                        String ref = "restaurantes/menu/" + itemName + ".jpg";
                                        StorageReference itemRef = storageRef.child(ref);
                                        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                promotion.setUri(uri);
                                                adapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                }
                                adapter.notifyDataSetChanged();
                            }
                                dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }


    private int aux;
    private void storePromotionsReservation(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                .document(ReservationOrderInstance.getInstance().getReservationId())
                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(getString(R.string.DATABASE_QUANTITY_FIELD));
                    Promotion promotion = new Promotion(
                            doc.getString(getString(R.string.DATABASE_MENU_FIELD)),
                            quantidade.intValue(),
                            doc.getString(getString(R.string.DATABASE_PROMOTION_FIELD)),
                            doc.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(getString(R.string.DATABASE_PRICE_FIELD))
                    );
                    int pos = ReservationOrderInstance.getInstance().getPosition(promotion);
                    if(pos != -1){
                        int quantidade1 = ReservationOrderInstance.getInstance().getPromotions().get(pos).getQuantidade();
                        if(quantidade1 != promotion.getQuantidade()){
                            ReservationOrderInstance.getInstance().getPromotions().remove(pos);
                            db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                                    .document(ReservationOrderInstance.getInstance().getReservationId())
                                    .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).document(doc.getId()).update(getString(R.string.DATABASE_QUANTITY_FIELD), quantidade1);
                        }else{
                            ReservationOrderInstance.getInstance().getPromotions().remove(pos);
                        }
                    }else{
                        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                                .document(ReservationOrderInstance.getInstance().getReservationId())
                                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).document(doc.getId()).delete();
                    }
                }
                addNewPromotionsReservation();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
            }
        });
    }

    private void addNewPromotionsReservation(){
        aux = 0;
        dialogLoading.show();
        if(ReservationOrderInstance.getInstance().getPromotions().size() == 0){
            dialogLoading.dismiss();
            finish();
        }

        for (Promotion promotion : ReservationOrderInstance.getInstance().getPromotions()){
            db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                    .document(ReservationOrderInstance.getInstance().getReservationId())
                    .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).add(promotion).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    aux++;
                    if(aux == ReservationOrderInstance.getInstance().getOrders().size()){
                        dialogLoading.dismiss();
                        finish();
                    }
                }
            });
        }
    }


    private void storePromotionsTakeaway(){
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots){
                    Number quantidade = (Number) doc.get(getString(R.string.DATABASE_QUANTITY_FIELD));
                    Promotion promotion = new Promotion(
                            doc.getString(getString(R.string.DATABASE_MENU_FIELD)),
                            quantidade.intValue(),
                            doc.getString(getString(R.string.DATABASE_PROMOTION_FIELD)),
                            doc.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                            (Number) doc.get(getString(R.string.DATABASE_PRICE_FIELD))
                    );
                    int pos = TakeawayOrderInstance.getInstance().getPosition(promotion);
                    if(pos != -1){
                        int quantidade1 = TakeawayOrderInstance.getInstance().getPromotions().get(pos).getQuantidade();
                        if(quantidade1 != promotion.getQuantidade()){
                            TakeawayOrderInstance.getInstance().getPromotions().remove(pos);
                            db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                                    .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                                    .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).document(doc.getId()).update(getString(R.string.DATABASE_QUANTITY_FIELD), quantidade1);
                        }else{
                            TakeawayOrderInstance.getInstance().getPromotions().remove(pos);
                        }
                    }else{
                        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                                .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).document(doc.getId()).delete();
                    }
                }
                addNewPromotionsTakeaway();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
            }
        });
    }

    private void addNewPromotionsTakeaway(){
        aux = 0;
        dialogLoading.show();
        if(TakeawayOrderInstance.getInstance().getPromotions().size() == 0){
            dialogLoading.dismiss();
            finish();
            return;
        }

        for (Promotion promotion : TakeawayOrderInstance.getInstance().getPromotions()){
            db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                    .document(TakeawayOrderInstance.getInstance().getTakeawayId())
                    .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).add(promotion).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    aux++;
                    if(aux == TakeawayOrderInstance.getInstance().getPromotions().size()){
                        dialogLoading.dismiss();
                        finish();
                    }
                }
            });
        }
    }
}
