package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.categories;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.menus.OrderMenuActivity;

public class CategoryAdaptar extends RecyclerView.Adapter<CategoryAdaptar.ViewHolder> {
    private List<String> categories;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewCategoryName;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewCategoryName = itemView.findViewById(R.id.textViewName);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public CategoryAdaptar(List<String> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_only_name, parent, false);
        return new CategoryAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdaptar.ViewHolder holder, int position) {
        String category = categories.get(position);

        holder.textViewCategoryName.setText(category);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderMenuActivity.class);
                intent.putExtra("category", category);
                context.startActivity(intent);
            }
        });

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
