package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Menu;

public class MenuAdaptar extends RecyclerView.Adapter<MenuAdaptar.ViewHolder> {
    private List<Menu> menus;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewMenu;
        public TextView textViewPrice;
        public TextView textViewIngredients;
        public CircleImageView imageViewMenu;
        public ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewMenu = itemView.findViewById(R.id.textViewMenu);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewIngredients = itemView.findViewById(R.id.textViewIngredients);
            textViewIngredients.setVisibility(View.GONE);
            constraintLayout = itemView.findViewById(R.id.constraintLayout);
            imageViewMenu = itemView.findViewById(R.id.imageViewMenu);
        }
    }

    public MenuAdaptar(List<Menu> menus, Context context) {
        this.menus = menus;
        this.context = context;
    }

    @NonNull
    @Override
    public MenuAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_menu, parent, false);
        return new MenuAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdaptar.ViewHolder holder, int position) {
        Menu currentItem = menus.get(position);

        Picasso.get().load(currentItem.getUri()).into(holder.imageViewMenu);
        holder.textViewMenu.setText(currentItem.getName());
        String price = currentItem.getPrice().toString() + " " + context.getString(R.string.Currency);
        holder.textViewPrice.setText(price);
        holder.textViewIngredients.setText(currentItem.getIngredients());

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MenuDetailsActivity.class);
                intent.putExtra("restaurant", currentItem.getRestaurant());
                intent.putExtra("category", currentItem.getCategory());
                intent.putExtra("menu", currentItem.getName());
                context.startActivity(intent);
            }
        });

        color++;
        switch (color % 3){
            case 0:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return menus.size();
    }
}
