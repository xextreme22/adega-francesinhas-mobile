package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import android.net.Uri;

public class Menu {
    private Uri uri;
    private String name;
    private String ingredients;
    private String restaurant;
    private Number price;
    private String category;

    public Menu(Uri uri, String name, String ingredients, String restaurant, Number price, String category) {
        this.uri = uri;
        this.name = name;
        this.ingredients = ingredients;
        this.restaurant = restaurant;
        this.price = price;
        this.category = category;
    }

    public Menu(String name, String ingredients, Number price) {
        this.uri = null;
        this.name = name;
        this.ingredients = ingredients;
        this.price = price;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public Number getPrice() {
        return price;
    }

    public void setPrice(Number price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
