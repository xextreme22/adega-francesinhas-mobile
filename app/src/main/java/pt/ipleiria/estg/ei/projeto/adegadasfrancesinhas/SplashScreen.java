package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.RegisterActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.User;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class SplashScreen extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;
    private StorageReference storageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        storageRef = FirebaseStorage.getInstance().getReference();
        userInfoInstance = UserInfoInstance.getInstance();

        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.CLOUD_MESSAGING_TOPIC_ALL));

        if(user == null){
            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                loadUserInfo();
            }else{
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void loadUserInfo(){
        String ref = "utilizadores/" + user.getUid() + "/avatar.jpg";
        StorageReference itemRef = storageRef.child(ref);
        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                UserInfoInstance.getInstance().setFoto(uri);
            }
        });

        DocumentReference userDoc = db.collection(getString(R.string.DATABASE_USERS_COLLECTION)).document(user.getUid());
        userDoc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()){
                        if(document.getBoolean(getString(R.string.DATABASE_DISABLED_FIELD))){
                            mAuth.signOut();
                            userInfoInstance.setUser(null);
                            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }else{
                            FirebaseMessaging.getInstance().subscribeToTopic(user.getUid());

                            userInfoInstance.setUser(new User(
                                    document.getString(getString(R.string.DATABASE_NAME_FIELD)),
                                    document.getString(getString(R.string.DATABASE_PHONE_NUMBER_FIELD)),
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_POINTS_FIELD)).toString()),
                                    document.getString(getString(R.string.DATABASE_ROLE_FIELD)),
                                    document.getBoolean(getString(R.string.DATABASE_DISABLED_FIELD))
                            ));

                            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }else{
                        Intent intent = new Intent(SplashScreen.this, RegisterActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }else{
                    DialogInfo dialogInfo = new DialogInfo();
                    AlertDialog alertDialog = dialogInfo.infoDialog(SplashScreen.this, getLayoutInflater(), getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    alertDialog.show();
                }
            }
        });
    }
}
