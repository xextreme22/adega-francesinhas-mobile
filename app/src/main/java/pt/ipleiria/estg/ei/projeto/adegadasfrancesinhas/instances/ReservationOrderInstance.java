package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Reservation;

public class ReservationOrderInstance {
    private static ReservationOrderInstance INSTANCE = null;

    private Reservation reservation;
    private String reservationId;
    private ArrayList<Order> orders;
    private ArrayList<Promotion> promotions;
    private Number total;

    public ReservationOrderInstance() {
        this.reservation = null;
        this.reservationId = null;
        this.total = 0.00;
        this.orders = new ArrayList<>();
        this.promotions = new ArrayList<>();
    }

    public static ReservationOrderInstance getInstance() {
        if(INSTANCE == null){
            INSTANCE = new ReservationOrderInstance();
            TakeawayOrderInstance.getInstance().resetAll();
        }
        return INSTANCE;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public ArrayList<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(ArrayList<Promotion> promotions) {
        this.promotions = promotions;
    }

    public Number getTotal() {
        return total;
    }

    public void setTotal(Number total) {
        this.total = total;
    }

    public int countOrder(Order order){
        for (Order aux: orders) {
            if(aux.getRestaurante().equals(order.getRestaurante())) {
                if (aux.getMenu().equals(order.getMenu())) {
                    if (aux.getCategoria().equals(order.getCategoria())) {
                        return aux.getQuantidade();
                    }
                }
            }
        }
        return 0;
    }

    public int countPromotion(Promotion promotion){
        for (Promotion aux: promotions) {
            if(aux.getPromocao().equals(promotion.getPromocao())) {
                return aux.getQuantidade();
            }
        }
        return 0;
    }

    public int getPosition(Order order){
        int i = 0;
        for (Order aux: orders) {
            if(aux.getRestaurante().equals(order.getRestaurante())) {
                if (aux.getMenu().equals(order.getMenu())) {
                    if (aux.getCategoria().equals(order.getCategoria())) {
                        return i;
                    }
                }
            }
            i++;
        }
        return -1;
    }

    public int getPosition(Promotion promotion){
        int i = 0;
        for (Promotion aux: promotions) {
            if(aux.getPromocao().equals(promotion.getPromocao())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public void addOrder(Order order){
        for (Order aux: orders) {
            if(aux.getMenu().equals(order.getMenu()) && aux.getCategoria().equals(order.getCategoria()) && aux.getRestaurante().equals(order.getRestaurante())){
                for (int i = 0; i < order.getQuantidade(); i++) {
                    aux.incrementQuantity();
                }
                total = addDoubles2DecimalPlaces(total.doubleValue(), (order.getPreco().doubleValue() * order.getQuantidade()));
                return;
            }
        }
        total = addDoubles2DecimalPlaces(total.doubleValue(), (order.getPreco().doubleValue() * order.getQuantidade()));
        orders.add(order.clone());
    }

    private Number addDoubles2DecimalPlaces(Double d1, Double d2){
        BigDecimal bigDecimal = BigDecimal.valueOf(d1 + d2);
        return bigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

    private Number subtractDoubles2DecimalPlaces(Double d1, Double d2){
        BigDecimal bigDecimal = BigDecimal.valueOf(d1 - d2);
        return bigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

    public void removeOrder(Order order){
        for (Order aux: orders) {
            if(aux.getMenu().equals(order.getMenu()) && aux.getCategoria().equals(order.getCategoria()) && aux.getRestaurante().equals(order.getRestaurante())){
                if(aux.getQuantidade() == order.getQuantidade()){
                    total = subtractDoubles2DecimalPlaces(total.doubleValue(), (aux.getPreco().doubleValue() * aux.getQuantidade()));
                    orders.remove(aux);
                    return;
                }else{
                    for (int i = 0; i < order.getQuantidade(); i++) {
                        aux.decrementQuantity();
                    }
                    total = subtractDoubles2DecimalPlaces(total.doubleValue(), (aux.getPreco().doubleValue() * aux.getQuantidade()));
                    return;
                }
            }
        }
    }

    public void addPromotion(Promotion promotion){
        for (Promotion aux: promotions) {
            if(aux.getPromocao().equals(promotion.getPromocao())){
                for (int i = 0; i < promotion.getQuantidade(); i++) {
                    aux.incrementQuantity();
                }
                return;
            }
        }
        promotions.add(promotion.clone());
    }

    public void removePromotion(Promotion promotion){
        for (Promotion aux: promotions) {
            if(aux.getPromocao().equals(promotion.getPromocao())){
                if(aux.getQuantidade() == promotion.getQuantidade()){
                    promotions.remove(aux);
                }else{
                    for (int i = 0; i < promotion.getQuantidade(); i++) {
                        aux.decrementQuantity();
                    }
                }
            }
        }
    }


    public void resetOrders(){
        orders = new ArrayList<>();
    }

    public void resetPromotions(){
        promotions = new ArrayList<>();
    }

    public void resetTakeaway(){
        reservation = null;
    }
    public void resetTotal(){
        total = 0;
    }

    public void resetAll(){
        resetOrders();
        resetTakeaway();
        resetPromotions();
        reservationId = null;
        resetTotal();
    }
}
