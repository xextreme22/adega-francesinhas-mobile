package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.promotions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.PromotionItem;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class PromotionAdaptar extends RecyclerView.Adapter<PromotionAdaptar.ViewHolder> {
    private List<PromotionItem> promotionItems;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public CircleImageView imageView;
        public TextView textViewItemName;
        public TextView textViewRestaurantName;
        public TextView textViewPointPrice;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageViewItem);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
            textViewRestaurantName = itemView.findViewById(R.id.textViewRestaurantName);
            textViewPointPrice = itemView.findViewById(R.id.textViewPointPrice);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public PromotionAdaptar(List<PromotionItem> promotionItems, Context context) {
        this.promotionItems = promotionItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_promotion, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PromotionItem currentItem = promotionItems.get(position);


        Picasso.get().load(currentItem.getUri()).into(holder.imageView);
        holder.textViewItemName.setText(currentItem.getTextItemName());
        holder.textViewRestaurantName.setText(currentItem.getTextRestaurantName());
        String pricePoints = currentItem.getTextPointPrice() + " " + context.getString(R.string.Points);
        holder.textViewPointPrice.setText(pricePoints);

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return promotionItems.size();
    }

}
