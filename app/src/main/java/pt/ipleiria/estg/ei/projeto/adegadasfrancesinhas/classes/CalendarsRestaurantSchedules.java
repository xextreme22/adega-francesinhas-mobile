package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import com.google.firebase.Timestamp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarsRestaurantSchedules {

    private List<Calendar> listCalendars;
    private List<ScheduleTimes> listScheduleTimes;

    public CalendarsRestaurantSchedules() {
        listCalendars = new ArrayList<>();
        listScheduleTimes = new ArrayList<>();
    }

    public List<Calendar> getListCalendars() {
        return listCalendars;
    }

    public List<ScheduleTimes> getListScheduleTimes() {
        return listScheduleTimes;
    }

    public void addToListCalendarDates(Timestamp openDate, Timestamp closeDate){
        Calendar openCalendar = Calendar.getInstance();
        openCalendar.setTimeInMillis(openDate.toDate().getTime());

        Calendar closeCalendar = Calendar.getInstance();
        closeCalendar.setTimeInMillis(closeDate.toDate().getTime());

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MINUTE, 15);

        int aux= 0;
        //for schedules with diferente dates for opening and closing (when restaurante is open for example 48h straight)
        while(openCalendar.compareTo(closeCalendar) != 1){ //while openCalendar is befor closeCalendar
            if(currentCalendar.compareTo(openCalendar) == 1){ //se data currente for superior a data de abertura
                if(!sameDay(currentCalendar, openCalendar)){ //if current date is superior to openDate and not in the same day
                    openCalendar.add(Calendar.DAY_OF_MONTH, 1);
                    aux++;
                    continue;
                }
            }

            int position = listPostionCalendar(openCalendar);
            if(position == -1){
                Calendar calendarClone = (Calendar) openCalendar.clone();
                listCalendars.add(calendarClone);
                position = listCalendars.indexOf(calendarClone);
                listScheduleTimes.add(position, new ScheduleTimes());
            }

            if(aux==0){ //1st iteration always type data to 00:00 or to data
                if(sameDay(openCalendar, closeCalendar)){ //se primeira iteração e mesmo dia entao é data to data
                    if(currentCalendar.compareTo(openCalendar) == 1){ //se data currente for superior a data de abertura
                        listScheduleTimes.get(position).addTimepointsBetween((Calendar) currentCalendar.clone(), (Calendar) closeCalendar.clone());
                    }else{
                        listScheduleTimes.get(position).addTimepointsBetween((Calendar) openCalendar.clone(), (Calendar) closeCalendar.clone());
                    }
                }else{ //data to 00:00
                    if(currentCalendar.compareTo(openCalendar) == 1){
                        listScheduleTimes.get(position).addTimepointsUntilEndDay((Calendar) currentCalendar.clone());
                    }else{
                        listScheduleTimes.get(position).addTimepointsUntilEndDay((Calendar) openCalendar.clone());
                    }
                }
            }else{
                if(sameDay(openCalendar, closeCalendar)){ //if we reached the end its type 00:00 to data
                    if(sameDay(currentCalendar, openCalendar)){
                        listScheduleTimes.get(position).addTimepointsBetween((Calendar) currentCalendar.clone(), (Calendar) closeCalendar.clone());
                    }else {
                        listScheduleTimes.get(position).addTimepointsFromStartDay((Calendar) closeCalendar.clone());
                    }
                }else{ //if we haven't reached the end and it isn't the 1st iteration then its type 00:00 to 00:00
                    if(sameDay(currentCalendar, openCalendar)){
                        listScheduleTimes.get(position).addTimepointsUntilEndDay((Calendar) currentCalendar.clone());
                    }else {
                        listScheduleTimes.get(position).addTimepointsCompleteDay();
                    }
                }
            }

            openCalendar.add(Calendar.DAY_OF_MONTH, 1);
            aux++;
        }
    }

    public int getPositionListCalendars (Calendar calendarDay){
        int position = 0;
        for (Calendar calendar: listCalendars) {
            if(calendar.get(Calendar.DAY_OF_YEAR) == calendarDay.get(Calendar.DAY_OF_YEAR) && calendar.get(Calendar.YEAR) == calendarDay.get(Calendar.YEAR)){
                break;
            }else{
                position++;
            }
        }

        return position;
    }

    public Boolean sameDay (Calendar calendar1, Calendar calendar2){
        return (calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR) && calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR));
    }

    public int listPostionCalendar (Calendar calendar1){
        for (Calendar calendar : listCalendars){
            if(sameDay(calendar, calendar1)){
                return listCalendars.indexOf(calendar);
            }
        }
        return -1;
    }
}
