package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

public class Order {
    private int quantidade;
    private String menu;
    private String categoria;
    private String restaurante;
    private Number preco;

    public Order(int quantidade, String menu, String categoria, String restaurante, Number preco) {
        this.quantidade = quantidade;
        this.menu = menu;
        this.categoria = categoria;
        this.restaurante = restaurante;
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(String restaurante) {
        this.restaurante = restaurante;
    }

    public Number getPreco() {
        return preco;
    }

    public void setPreco(Number preco) {
        this.preco = preco;
    }

    public void decrementQuantity(){
        this.quantidade--;
    }

    public void incrementQuantity(){
        this.quantidade++;
    }

    public Order clone(){
        return new Order(quantidade, menu, categoria, restaurante, preco);
    }
}
