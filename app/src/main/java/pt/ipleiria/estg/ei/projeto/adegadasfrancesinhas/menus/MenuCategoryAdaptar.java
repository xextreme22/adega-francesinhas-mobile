package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class MenuCategoryAdaptar extends RecyclerView.Adapter<MenuCategoryAdaptar.ViewHolder> {
    private List<String> categories;
    private Context context;
    private String restaurant;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewCategoryName;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewCategoryName = itemView.findViewById(R.id.textViewName);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public MenuCategoryAdaptar(List<String> categories, String restaurant, Context context) {
        this.categories = categories;
        this.restaurant = restaurant;
        this.context = context;
    }

    @NonNull
    @Override
    public MenuCategoryAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_only_name, parent, false);
        return new MenuCategoryAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuCategoryAdaptar.ViewHolder holder, int position) {
        String category = categories.get(position);

        holder.textViewCategoryName.setText(category);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MenuActivity.class);
                intent.putExtra("restaurant", restaurant);
                intent.putExtra("category", category);
                context.startActivity(intent);
            }
        });

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
