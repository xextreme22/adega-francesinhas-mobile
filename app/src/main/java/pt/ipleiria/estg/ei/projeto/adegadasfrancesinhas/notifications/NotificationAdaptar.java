package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.notifications;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Notification;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class NotificationAdaptar extends RecyclerView.Adapter<NotificationAdaptar.ViewHolder> {
    private List<Notification> notifications;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewTitle;
        public EditText editTextBody;
        public TextView textViewDate;
        public ImageView imageView;
        public ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            editTextBody = itemView.findViewById(R.id.editTextBody);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            constraintLayout = itemView.findViewById(R.id.constraintLayout);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }

    public NotificationAdaptar(List<Notification> notifications, Context context) {
        this.notifications = notifications;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_notification, parent, false);
        return new NotificationAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdaptar.ViewHolder holder, int position) {
        Notification currentNotification = notifications.get(position);

        Picasso.get().load(currentNotification.getUri()).into(holder.imageView);
        holder.textViewTitle.setText(currentNotification.getTitle());
        holder.editTextBody.setText(currentNotification.getBody());
        holder.textViewDate.setText( DateFormat.format( context.getString(R.string.Date_format), currentNotification.getDate().toDate()));

        color++;
        switch (color % 3){
            case 0:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }
}
