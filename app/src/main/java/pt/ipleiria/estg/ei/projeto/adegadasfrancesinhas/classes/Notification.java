package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import android.net.Uri;

import com.google.firebase.Timestamp;

public class Notification {
    private Uri uri;
    private String id;
    private String title;
    private String body;
    private Timestamp date;

    public Notification(Uri uri, String id, String title, String body, Timestamp date) {
        this.uri = uri;
        this.id = id;
        this.title = title;
        this.body = body;
        this.date = date;
    }

    public Notification(String id, String title, String body, Timestamp date) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
