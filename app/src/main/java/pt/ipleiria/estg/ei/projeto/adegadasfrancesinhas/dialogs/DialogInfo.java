package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class DialogInfo {
    private TextView textViewTitle;
    private TextView textViewInformation;
    public Button buttonOk;

    public AlertDialog infoDialog(Context context, LayoutInflater layoutInflater, String title, String body){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View mView = layoutInflater.inflate(R.layout.dialog_message, null);
        textViewTitle =  mView.findViewById(R.id.textViewTitle);
        textViewInformation =  mView.findViewById(R.id.textViewInformation);
        buttonOk = mView.findViewById(R.id.buttonOk);

        textViewTitle.setText(title);
        textViewInformation.setText(body);

        mBuilder.setView(mView);
        AlertDialog dialogError = mBuilder.create();

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogError.hide();
            }
        });

        return dialogError;
    }
}
