package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import android.net.Uri;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class User {
    private String nome;
    private String telemovel;
    private Number pontos;
    private String role;
    private Boolean disabled;

    public User(String nome, String telemovel, String role) {
        this.nome = nome;
        this.telemovel = telemovel;
        this.role = role;
        this.pontos = 0;
        this.disabled = false;
    }

    public User(String nome, String telemovel, Number pontos, String role, Boolean disabled) {
        this.nome = nome;
        this.telemovel = telemovel;
        this.pontos = pontos;
        this.role = role;
        this.disabled = disabled;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelemovel() {
        return telemovel;
    }

    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }

    public Number getPontos() {
        return pontos;
    }

    public void setPontos(Number pontos) {
        this.pontos = pontos;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public void addPontos(int pontos){
        this.pontos = this.pontos.intValue() + pontos;
    }

    public void removePontos(int pontos){
        this.pontos = this.pontos.intValue() - pontos;
    }
}
