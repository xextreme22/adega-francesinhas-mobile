package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.restaurants;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class RestaurantAdaptar extends RecyclerView.Adapter<RestaurantAdaptar.ViewHolder> {
    private List<String> restaurants;
    private Context context;
    private LayoutInflater layoutInflater;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewRestaurantName;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewRestaurantName = itemView.findViewById(R.id.textViewName);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public RestaurantAdaptar(List<String> restaurants, Context context, LayoutInflater layoutInflater) {
        this.restaurants = restaurants;
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @NonNull
    @Override
    public RestaurantAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_only_name, parent, false);
        return new RestaurantAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantAdaptar.ViewHolder holder, int position) {
        String currentRestaurant = restaurants.get(position);

        holder.textViewRestaurantName.setText(currentRestaurant);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.relativeLayout.setEnabled(false);
                Intent intent = new Intent(context, RestaurantDetailsActivity.class);
                intent.putExtra("restaurant", currentRestaurant);
                context.startActivity(intent);
                holder.relativeLayout.setEnabled(true);
            }
        });

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }
}
