package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.previous;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class PreviousActivity extends AppCompatActivity {
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private Button buttonBack;
    private TextView textViewTitle;
    private TextView textViewNoItems;

    private List<List<Order>> previousOrders = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String restaurant;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_recycler);

        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            restaurant = TakeawayOrderInstance.getInstance().getTakeaway().getRestaurante();
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            restaurant = ReservationOrderInstance.getInstance().getReservation().getRestaurante();
        }

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        buildRecyclerView();

        dialogLoading = DialogLoading.loadingDialog(PreviousActivity.this, getLayoutInflater());

        buttonBack = findViewById(R.id.buttonBack);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        textViewNoItems.setText(getString(R.string.INFO_NO_PREVIOUS_ORDERS));
        textViewTitle.setText(getString(R.string.Previous_order));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PreviousAdaptar(previousOrders, PreviousActivity.this, getLayoutInflater());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    private int aux;
    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(PreviousActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(PreviousActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    dialogLoading.show();
                    aux=0;
                    loadPreviousTakeaways();
                    loadPreviousReservations();
                }
            }else{
                Intent intent = new Intent(PreviousActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }


    private void popupDialogError(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(PreviousActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    private void loadPreviousTakeaways(){
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MONTH, -1);
        Timestamp timestamp = new Timestamp(new Date(currentCalendar.getTimeInMillis()));
        db.collection(getString(R.string.DATABASE_TAKEAWAY_COLLECTION))
                .whereGreaterThanOrEqualTo(getString(R.string.DATABASE_DATE_FIELD), timestamp)
                .whereEqualTo(getString(R.string.DATABASE_USER_FIELD), user.getUid())
                .whereEqualTo(getString(R.string.DATABASE_RESTAURANT_FIELD), restaurant)
                .whereEqualTo(getString(R.string.DATABASE_PAYED_FIELD), true)
                .orderBy(getString(R.string.DATABASE_DATE_FIELD), Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult() != null){
                                if(task.getResult().isEmpty()){
                                    if(aux==1){
                                        textViewNoItems.setVisibility(View.VISIBLE);
                                        dialogLoading.dismiss();
                                    }else{
                                        aux++;
                                    }
                                    return;
                                }
                                textViewNoItems.setVisibility(View.GONE);

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ArrayList<Order> orders = new ArrayList<Order>();
                                    previousOrders.add(orders);
                                    loadOrder(document.getId(), getString(R.string.DATABASE_TAKEAWAY_COLLECTION), orders);
                                }
                            }
                        } else {
                            if(task.getException() != null){
                                popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                            }
                        }
                    }
                });
    }

    private void loadPreviousReservations(){
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.MONTH, -1);
        Timestamp timestamp = new Timestamp(new Date(currentCalendar.getTimeInMillis()));
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                .whereGreaterThanOrEqualTo(getString(R.string.DATABASE_DATE_FIELD), timestamp)
                .whereEqualTo(getString(R.string.DATABASE_USER_FIELD), user.getUid())
                .whereEqualTo(getString(R.string.DATABASE_RESTAURANT_FIELD), restaurant)
                .whereEqualTo(getString(R.string.DATABASE_PAYED_FIELD), true)
                .orderBy(getString(R.string.DATABASE_DATE_FIELD), Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult() != null) {
                                if (task.getResult().isEmpty()) {
                                    if (aux == 1) {
                                        textViewNoItems.setVisibility(View.VISIBLE);
                                        dialogLoading.dismiss();
                                    } else {
                                        aux++;
                                    }
                                    return;
                                }
                                textViewNoItems.setVisibility(View.GONE);

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ArrayList<Order> orders = new ArrayList<Order>();
                                    previousOrders.add(orders);
                                    loadOrder(document.getId(), getString(R.string.DATABASE_RESERVATION_COLLECTION), orders);
                                }
                            }
                        } else {
                            if(task.getException() != null){
                                popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                            }
                        }
                    }
                });
    }

    private void loadOrder(String id, String path, ArrayList orders){
        db.collection(path)
                .document(id)
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION))
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Order order = new Order(
                                        Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                        document.getString(getString(R.string.DATABASE_MENU_FIELD)),
                                        document.getString(getString(R.string.DATABASE_CATEGORY_FIELD)),
                                        document.getString(getString(R.string.DATABASE_RESTAURANT_FIELD)),
                                        (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))
                                );
                                getMenu(order, orders);
                            }
                        } else {
                            popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_MESSAGE) + task.getException().getMessage());
                        }
                    }
                });
    }


    private void getMenu(Order order, ArrayList orders){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(order.getRestaurante())
                .collection(getString(R.string.DATABASE_CATEGORY_COLLECTION))
                .document(order.getCategoria())
                .collection(getString(R.string.DATABASE_ITEMS_COLLECTION))
                .document(order.getMenu())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            if(documentSnapshot.get("disabled") != null){
                                if(!documentSnapshot.getBoolean("disabled")){
                                    orders.add(new Order(
                                            order.getQuantidade(),
                                            order.getMenu(),
                                            order.getCategoria(),
                                            order.getRestaurante(),
                                            (Number) documentSnapshot.get(getString(R.string.DATABASE_PRICE_FIELD))
                                    ));
                                    adapter.notifyDataSetChanged();
                                    dialogLoading.dismiss();
                                }
                            }
                        }
                    }
                });
    }
}
