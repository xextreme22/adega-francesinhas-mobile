package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.promotions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.LinkedList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.PromotionItem;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class PromotionActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;
    private UserInfoInstance userInfoInstance;

    private LinkedList<PromotionItem> promotionsList = new LinkedList<>();

    private TextView textViewPoints;
    private TextView textViewNoItems;
    private Button buttonBack;
    private ImageView imageViewNumberOfPoints;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        buildRecyclerView();

        dialogLoading = DialogLoading.loadingDialog(PromotionActivity.this, getLayoutInflater());

        buttonBack = findViewById(R.id.buttonBack);
        textViewPoints = findViewById(R.id.textViewPoints);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        imageViewNumberOfPoints = findViewById(R.id.imageViewNumberOfPoints);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imageViewNumberOfPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInfo dialogInfo = new DialogInfo();
                AlertDialog alertDialog = dialogInfo.infoDialog(
                        PromotionActivity.this,
                        getLayoutInflater(),
                        getString(R.string.Information),
                        getString(R.string.INFO_HOW_TO_OBTAIN_POINTS));
                dialogInfo.buttonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(PromotionActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PromotionAdaptar(promotionsList, PromotionActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(PromotionActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(PromotionActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    textViewPoints.setText(userInfoInstance.getUser().getPontos().toString());
                    if(promotionsList.size() == 0){
                        dialogLoading.show();
                        loadList();
                    }
                }
            }else{
                Intent intent = new Intent(PromotionActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void loadList(){
        db.collection(getString(R.string.DATABASE_PROMOTION_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .orderBy(getString(R.string.DATABASE_POINTS_FIELD))
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else{
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                        final String restaurante = document.get(getString(R.string.DATABASE_RESTAURANT_FIELD)).toString();
                                        final String item = document.get(getString(R.string.DATABASE_ITEM_FIELD)).toString();
                                        final String pontos = document.get(getString(R.string.DATABASE_POINTS_FIELD)).toString();

                                        PromotionItem promotion = new PromotionItem(item, restaurante, pontos);
                                        promotionsList.add(promotion);

                                        String ref = "restaurantes/menu/" + item + ".jpg";
                                        StorageReference itemRef = storageRef.child(ref);
                                        itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                promotion.setUri(uri);
                                                adapter.notifyDataSetChanged();
                                            }
                                        });
                                }
                                adapter.notifyDataSetChanged();
                            }
                                dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }

}
