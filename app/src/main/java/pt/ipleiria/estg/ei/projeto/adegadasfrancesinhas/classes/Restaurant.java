package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes;

import android.net.Uri;

public class Restaurant {
    private Uri uri;
    private String name;
    private String scheduleDescription;
    private String telephone;
    private Number lat;
    private Number lon;

    public Restaurant(String name) {
        this.uri = null;
        this.name = name;
        this.scheduleDescription = null;
        this.telephone = null;
        this.lat = null;
        this.lon = null;
    }

    public Restaurant(String name, String scheduleDescription, String telephone, Number lat, Number lon) {
        this.uri = null;
        this.name = name;
        this.scheduleDescription = scheduleDescription;
        this.telephone = telephone;
        this.lat = lat;
        this.lon = lon;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScheduleDescription() {
        return scheduleDescription;
    }

    public void setScheduleDescription(String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Number getLat() {
        return lat;
    }

    public void setLat(Number lat) {
        this.lat = lat;
    }

    public Number getLon() {
        return lon;
    }

    public void setLon(Number lon) {
        this.lon = lon;
    }
}
