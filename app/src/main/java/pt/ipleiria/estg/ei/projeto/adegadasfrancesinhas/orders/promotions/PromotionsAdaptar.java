package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.promotions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.UserInfo;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class PromotionsAdaptar extends RecyclerView.Adapter<PromotionsAdaptar.ViewHolder> {
    private List<PromotionItem> promotionItems;
    private TextView textViewTotalNumberPoints;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public CircleImageView imageView;
        public TextView textViewItemName;
        public TextView textViewPointPrice;
        public RelativeLayout relativeLayout;
        public Button buttonIncrease;
        public Button buttonDecrease;
        public Button buttonQuantity;

        private int quantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageViewItem);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
            textViewPointPrice = itemView.findViewById(R.id.textViewPointPrice);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            buttonIncrease = itemView.findViewById(R.id.buttonIncrease);
            buttonDecrease = itemView.findViewById(R.id.buttonDecrease);
            buttonQuantity = itemView.findViewById(R.id.buttonQuantity);
        }
    }

    public PromotionsAdaptar(List<PromotionItem> promotionItems, TextView textViewTotalNumberPoints, Context context) {
        this.promotionItems = promotionItems;
        this.textViewTotalNumberPoints = textViewTotalNumberPoints;
        this.context = context;
    }

    @NonNull
    @Override
    public PromotionsAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_order_promotion, parent, false);
        return new PromotionsAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PromotionsAdaptar.ViewHolder holder, int position) {
        PromotionItem currentItem = promotionItems.get(position);

        Promotion promotion = new Promotion(currentItem.getNameItem(), 1, currentItem.getPromotionId(), currentItem.getRestaurant(), Integer.valueOf(currentItem.getPricePoints()));
        int quantity = 0;
        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            quantity = TakeawayOrderInstance.getInstance().countPromotion(promotion);
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            quantity = ReservationOrderInstance.getInstance().countPromotion(promotion);
        }
        holder.quantity = quantity;
        holder.buttonQuantity.setText(String.valueOf(quantity));

        holder.buttonQuantity.setText(String.valueOf(quantity));
        Picasso.get().load(currentItem.getUri()).into(holder.imageView);
        holder.textViewItemName.setText(currentItem.getNameItem());
        String pricePoints = currentItem.getPricePoints() + " " + context.getString(R.string.Points);
        holder.textViewPointPrice.setText(pricePoints);

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }

        holder.buttonIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserInfoInstance.getInstance().getUser().getPontos().intValue() < promotion.getPreco().intValue()){
                    return;
                }
                holder.buttonIncrease.setEnabled(false);
                holder.buttonDecrease.setEnabled(false);
                UserInfoInstance.getInstance().getUser().removePontos(promotion.getPreco().intValue());
                textViewTotalNumberPoints.setText(UserInfoInstance.getInstance().getUser().getPontos().toString());

                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    TakeawayOrderInstance.getInstance().addPromotion(promotion);
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    ReservationOrderInstance.getInstance().addPromotion(promotion);
                }

                holder.quantity++;
                holder.buttonQuantity.setText(String.valueOf(holder.quantity));
                holder.buttonIncrease.setEnabled(true);
                holder.buttonDecrease.setEnabled(true);
            }
        });

        holder.buttonDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.quantity <= 0){
                    return;
                }
                holder.buttonIncrease.setEnabled(false);
                holder.buttonDecrease.setEnabled(false);
                UserInfoInstance.getInstance().getUser().addPontos(promotion.getPreco().intValue());
                textViewTotalNumberPoints.setText(UserInfoInstance.getInstance().getUser().getPontos().toString());

                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    TakeawayOrderInstance.getInstance().removePromotion(promotion);
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    ReservationOrderInstance.getInstance().removePromotion(promotion);
                }

                holder.quantity--;
                holder.buttonQuantity.setText(String.valueOf(holder.quantity));
                holder.buttonIncrease.setEnabled(true);
                holder.buttonDecrease.setEnabled(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promotionItems.size();
    }
}
