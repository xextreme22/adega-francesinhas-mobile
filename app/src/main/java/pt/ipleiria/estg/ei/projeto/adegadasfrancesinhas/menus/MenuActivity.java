package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Menu;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;

public class MenuActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference storageRef;
    private String category;
    private String restaurant;

    private ArrayList<Menu> menus = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private Button buttonBack;
    private TextView textViewTitle;
    private TextView textViewNoItems;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_recycler);

        restaurant = getIntent().getStringExtra("restaurant");
        category = getIntent().getStringExtra("category");

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        storageRef = FirebaseStorage.getInstance().getReference();

        dialogLoading = DialogLoading.loadingDialog(MenuActivity.this, getLayoutInflater());

        buttonBack = findViewById(R.id.buttonBack);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewNoItems = findViewById(R.id.textViewNoItems);
        textViewNoItems.setText(getString(R.string.INFO_NO_MENUS));
        textViewTitle.setText(category);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            buildRecyclerView();
            if(menus.size() == 0){
                dialogLoading.show();
                loadMenus();
            }
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(MenuActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new MenuAdaptar(menus, MenuActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void loadMenus(){
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION))
                .document(restaurant)
                .collection(getString(R.string.DATABASE_CATEGORY_COLLECTION))
                .document(category)
                .collection(getString(R.string.DATABASE_ITEMS_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty()){
                                textViewNoItems.setVisibility(View.VISIBLE);
                            }else {
                                textViewNoItems.setVisibility(View.GONE);
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    String menuName = document.getId();
                                    Number price = (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD));
                                    String ingredients = document.get(getString(R.string.DATABASE_INGREDIENTS_FIELD)).toString();

                                    String ref = "restaurantes/menu/" + menuName + ".jpg";
                                    StorageReference itemRef = storageRef.child(ref);
                                    itemRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            menus.add(new Menu(uri, menuName, ingredients, restaurant, price, category));
                                            adapter.notifyDataSetChanged();
                                        }
                                    });
                                }
                            }
                            dialogLoading.dismiss();
                        } else {
                            dialogLoading.dismiss();
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }

}
