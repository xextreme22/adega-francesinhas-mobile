package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.SplashScreen;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogYesNo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.menus.MenuRestaurantsActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.notifications.NotificationsActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.profile.ProfileActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.promotions.PromotionActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations.ReservationActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.restaurants.RestaurantsActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.takeaways.TakeawayActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;

public class MainActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;

    private Button buttonProfile;
    private Button buttonNotifications;
    private ImageView imageReservations;
    private ImageView imageRestaurants;
    private ImageView imageTakeaways;
    private ImageView imageMenus;
    private ImageView imagePoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();

        buttonProfile = findViewById(R.id.buttonProfile);
        buttonNotifications = findViewById(R.id.buttonNotifications);
        imageReservations = findViewById(R.id.imageViewReservations);
        imageRestaurants = findViewById(R.id.imageViewRestaurants);
        imageTakeaways = findViewById(R.id.imageViewTakeaway);
        imageMenus = findViewById(R.id.imageViewMenu);
        imagePoints = findViewById(R.id.imageViewPoints);

        if(user != null){
            if(!user.isAnonymous()){
                FirebaseMessaging.getInstance().subscribeToTopic(user.getUid());
            }
        }

        buttonProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonProfile.setEnabled(false);
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
                buttonProfile.setEnabled(true);
            }
        });

        buttonNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonNotifications.setEnabled(false);
                Intent intent = new Intent(MainActivity.this, NotificationsActivity.class);
                startActivity(intent);
                buttonNotifications.setEnabled(true);
            }
        });

        imageRestaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageRestaurants.setEnabled(false);
                Intent intent = new Intent(MainActivity.this, RestaurantsActivity.class);
                startActivity(intent);
                imageRestaurants.setEnabled(true);
            }
        });

        imageMenus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageMenus.setEnabled(false);
                Intent intent = new Intent(MainActivity.this, MenuRestaurantsActivity.class);
                startActivity(intent);
                imageMenus.setEnabled(true);
            }
        });

        imagePoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePoints.setEnabled(false);
                if(user != null){
                    if(!user.isAnonymous()) {
                        if(userInfoInstance.getUser() != null) {
                            Intent intent = new Intent(MainActivity.this, PromotionActivity.class);
                            startActivity(intent);
                        }else{
                            noUserInformation();
                        }
                    }else{
                        popupAuthenticationRequired();
                    }
                }
                imagePoints.setEnabled(true);
            }
        });

        imageReservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageReservations.setEnabled(false);
                if(user != null){
                    if(!user.isAnonymous()) {
                        if(userInfoInstance.getUser() != null) {
                            Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                            startActivity(intent);
                        }else{
                            noUserInformation();
                        }
                    }else{
                        popupAuthenticationRequired();
                    }
                }
                imageReservations.setEnabled(true);
            }
        });

        imageTakeaways.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageTakeaways.setEnabled(false);
                if(user != null){
                    if(!user.isAnonymous()) {
                        if(userInfoInstance.getUser() != null) {
                            Intent intent = new Intent(MainActivity.this, TakeawayActivity.class);
                            startActivity(intent);
                        }else{
                            noUserInformation();
                        }
                    }else{
                        popupAuthenticationRequired();
                    }
                }
                imageTakeaways.setEnabled(true);
            }
        });
    }

    private void popupAuthenticationRequired(){
        DialogYesNo dialogYesNo = new DialogYesNo();
        AlertDialog alertDialogWarning = dialogYesNo.infoDialog(
                MainActivity.this,
                getLayoutInflater(),
                getString(R.string.WARNING),
                getString(R.string.WARNING_AUTHENTICATION_REQUIRED),
                getString(R.string.Login),
                getString(R.string.Cancel));

        dialogYesNo.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        dialogYesNo.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogWarning.dismiss();
            }
        });
        alertDialogWarning.show();
    }


    @Override
    protected void onStart() {
        super.onStart();

        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            TakeawayOrderInstance.getInstance().resetAll();
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            TakeawayOrderInstance.getInstance().resetAll();
        }

        if(user == null){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous() && UserInfoInstance.getInstance().getUser() == null) {
                 noUserInformation();
            }
        }
    }

    private void noUserInformation(){
        Intent intent = new Intent(MainActivity.this, SplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
