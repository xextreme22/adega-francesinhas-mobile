package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.menus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Menu;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.ReservationOrderInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.TakeawayOrderInstance;

public class OrderMenuAdaptar extends RecyclerView.Adapter<OrderMenuAdaptar.ViewHolder> {
    private List<Menu> menus;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewMenu;
        public TextView textViewPrice;
        public Button buttonIncrease;
        public Button buttonDecrease;
        public Button buttonQuantity;
        public CircleImageView imageViewMenu;
        public ConstraintLayout constraintLayout;

        private int quantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewMenu = itemView.findViewById(R.id.textViewMenu);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            buttonIncrease = itemView.findViewById(R.id.buttonIncrease);
            buttonDecrease = itemView.findViewById(R.id.buttonDecrease);
            buttonQuantity = itemView.findViewById(R.id.buttonQuantity);
            constraintLayout = itemView.findViewById(R.id.constraintLayout);
            imageViewMenu = itemView.findViewById(R.id.imageViewMenu);
        }
    }

    public OrderMenuAdaptar(List<Menu> menus, Context context) {
        this.menus = menus;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderMenuAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_order_menu, parent, false);
        return new OrderMenuAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderMenuAdaptar.ViewHolder holder, int position) {
        Menu currentItem = menus.get(position);

        Order order = new Order(1, currentItem.getName(), currentItem.getCategory(), currentItem.getRestaurant(), currentItem.getPrice());
        int quantity = 0;
        if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
            quantity = TakeawayOrderInstance.getInstance().countOrder(order);
        }
        if(ReservationOrderInstance.getInstance().getReservation() != null){
            quantity = ReservationOrderInstance.getInstance().countOrder(order);
        }
        holder.quantity = quantity;
        holder.buttonQuantity.setText(String.valueOf(quantity));

        Picasso.get().load(currentItem.getUri()).into(holder.imageViewMenu);
        holder.textViewMenu.setText(currentItem.getName());
        String price = currentItem.getPrice().toString() + " " + context.getString(R.string.Currency);
        holder.textViewPrice.setText(price);

        color++;
        switch (color % 3){
            case 0:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.constraintLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }

        holder.buttonIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.buttonIncrease.setEnabled(false);
                holder.buttonDecrease.setEnabled(false);
                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    TakeawayOrderInstance.getInstance().addOrder(order);
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    ReservationOrderInstance.getInstance().addOrder(order);
                }
                holder.quantity++;
                holder.buttonQuantity.setText(String.valueOf(holder.quantity));
                holder.buttonIncrease.setEnabled(true);
                holder.buttonDecrease.setEnabled(true);
            }
        });

        holder.buttonDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.buttonDecrease.setEnabled(false);
                if(holder.quantity <= 0){
                    return;
                }
                holder.buttonIncrease.setEnabled(false);
                if(TakeawayOrderInstance.getInstance().getTakeaway() != null){
                    TakeawayOrderInstance.getInstance().removeOrder(order);
                }
                if(ReservationOrderInstance.getInstance().getReservation() != null){
                    ReservationOrderInstance.getInstance().removeOrder(order);
                }
                holder.quantity--;
                holder.buttonQuantity.setText(String.valueOf(holder.quantity));
                holder.buttonIncrease.setEnabled(true);
                holder.buttonDecrease.setEnabled(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return menus.size();
    }
}
