package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogYesNo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Order;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.OrderAdaptar;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders.PromotionAdaptar;

public class DetailsReservationActivity extends AppCompatActivity {

    private Button buttonBack;
    private Button buttonPay;
    private Button buttonCancel;
    private TextView textViewTitle;
    private TextView textViewComment;
    private TextView textViewOrderTitle;
    private ImageView imageViewOrderTitleBar;
    private ImageView imageViewPromotion;
    private ImageView imageViewMenu;
    private ImageView imageViewPhoneCall;
    private ImageView imageViewBottomSeparator;
    private ImageView imageViewCommentBackground;
    private EditText editTextComment;

    private String idReservation;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;

    private ArrayList<Order> orderItems;
    private RecyclerView recyclerViewOrders;
    private RecyclerView.Adapter adapterOrder;
    private RecyclerView.LayoutManager layoutManagerOrder;

    private ArrayList<Promotion> promotionItems;
    private RecyclerView recyclerViewPromotions;
    private RecyclerView.Adapter adapterPromotion;
    private RecyclerView.LayoutManager layoutManagerPromotion;

    private AlertDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        userInfoInstance = UserInfoInstance.getInstance();

        idReservation = getIntent().getStringExtra("id");

        buttonBack = findViewById(R.id.buttonBack);
        buttonPay = findViewById(R.id.buttonPay);
        imageViewPromotion = findViewById(R.id.imageViewPromotion);
        imageViewMenu = findViewById(R.id.imageViewMenu);
        imageViewPhoneCall = findViewById(R.id.imageViewAux);
        textViewOrderTitle = findViewById(R.id.textViewOrderTitle);
        imageViewOrderTitleBar = findViewById(R.id.imageViewOrderTitleBar);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewComment = findViewById(R.id.textViewComment);
        editTextComment = findViewById(R.id.editTextComment);
        buttonCancel = findViewById(R.id.buttonCancel);
        imageViewCommentBackground = findViewById(R.id.imageViewCommentBackground);

        dialogLoading = DialogLoading.loadingDialog(this, getLayoutInflater());

        textViewTitle.setText(getString(R.string.Order_Details));
        imageViewPhoneCall.setImageResource(R.drawable.icon_phone_white);

        buttonCancel.setVisibility(View.VISIBLE);
        buttonPay.setVisibility(View.GONE);
        imageViewPromotion.setVisibility(View.GONE);
        imageViewMenu.setVisibility(View.GONE);
        imageViewPhoneCall.setVisibility(View.GONE);
        editTextComment.setVisibility(View.GONE);
        textViewComment.setVisibility(View.GONE);
        imageViewCommentBackground.setVisibility(View.GONE);
        imageViewOrderTitleBar.setVisibility(View.GONE);
        textViewOrderTitle.setVisibility(View.GONE);

        editTextComment.clearFocus();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelReservation();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(DetailsReservationActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                dialogLoading.show();
                loadReservation();
                promotionItems = new ArrayList<>();
                orderItems = new ArrayList<>();
                buildRecyclerViews();
                loadOrders();
                loadPromotions();
            }else{
                Intent intent = new Intent(DetailsReservationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DetailsReservationActivity.this, ReservationActivity.class);
        startActivity(intent);
    }

    private void popupDialogError(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(DetailsReservationActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void buildRecyclerViews(){
        //promotions
        recyclerViewPromotions = findViewById(R.id.recyclerViewPromotions);
        recyclerViewPromotions.setHasFixedSize(false);
        layoutManagerPromotion = new LinearLayoutManager(this);
        adapterPromotion = new PromotionAdaptar(promotionItems, DetailsReservationActivity.this);
        recyclerViewPromotions.setLayoutManager(layoutManagerPromotion);
        recyclerViewPromotions.setAdapter(adapterPromotion);

        //orders
        recyclerViewOrders = findViewById(R.id.recyclerViewOrders);
        recyclerViewOrders.setHasFixedSize(false);
        layoutManagerOrder = new LinearLayoutManager(this);
        adapterOrder = new OrderAdaptar(orderItems, DetailsReservationActivity.this);
        recyclerViewOrders.setLayoutManager(layoutManagerOrder);
        recyclerViewOrders.setAdapter(adapterOrder);
    }

    public void loadReservation(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).document(idReservation).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot result = task.getResult();
                    String comentario = result.get(getString(R.string.DATABASE_COMMENT_FIELD)).toString();
                    if(!comentario.equals("")){
                        editTextComment.setText(comentario);
                        editTextComment.setVisibility(View.VISIBLE);
                        textViewComment.setVisibility(View.VISIBLE);
                        imageViewCommentBackground.setVisibility(View.VISIBLE);
                        imageViewOrderTitleBar.setVisibility(View.VISIBLE);
                        textViewOrderTitle.setVisibility(View.VISIBLE);
                    }
                    dialogLoading.dismiss();
                } else {
                    dialogLoading.dismiss();
                    popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadPromotions(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).document(idReservation)
                .collection(getString(R.string.DATABASE_PROMOTION_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            promotionItems.add(new Promotion(
                                    document.get(getString(R.string.DATABASE_MENU_FIELD)).toString(),
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.get(getString(R.string.DATABASE_PROMOTION_FIELD)).toString(),
                                    document.get(getString(R.string.DATABASE_RESTAURANT_FIELD)).toString(),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))));
                            adapterPromotion.notifyDataSetChanged();
                        }
                    }
                }else{
                    popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadOrders(){
        db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).document(idReservation)
                .collection(getString(R.string.DATABASE_ORDER_COLLECTION)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    if(!task.getResult().isEmpty()){
                        for (DocumentSnapshot document : task.getResult().getDocuments()){
                            orderItems.add(new Order(
                                    Integer.parseInt(document.get(getString(R.string.DATABASE_QUANTITY_FIELD)).toString()),
                                    document.get(getString(R.string.DATABASE_MENU_FIELD)).toString(),
                                    document.get(getString(R.string.DATABASE_CATEGORY_FIELD)).toString(),
                                    document.get(getString(R.string.DATABASE_RESTAURANT_FIELD)).toString(),
                                    (Number) document.get(getString(R.string.DATABASE_PRICE_FIELD))));
                            adapterOrder.notifyDataSetChanged();
                        }
                    }
                }else{
                    popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    private void cancelReservation(){
        DialogYesNo dialogYesNo = new DialogYesNo();
        AlertDialog alertDialogWarning = dialogYesNo.infoDialog(
                DetailsReservationActivity.this,
                getLayoutInflater(),
                getString(R.string.WARNING),
                getString(R.string.WARNING_CANCEL_RESERVATION),
                getString(R.string.Yes),
                getString(R.string.No));
        dialogYesNo.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialogLoading = DialogLoading.loadingDialog(DetailsReservationActivity.this, getLayoutInflater());
                alertDialogWarning.dismiss();
                dialogLoading.show();
                db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION))
                        .document(idReservation)
                        .update(getString(R.string.DATABASE_CANCELLED_FIELD), true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialogLoading.dismiss();
                        Intent intent = new Intent(DetailsReservationActivity.this, ReservationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialogLoading.dismiss();
                        popupDialogError(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                    }
                });
            }
        });
        dialogYesNo.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogWarning.dismiss();
            }
        });
        alertDialogWarning.show();
    }
}
