package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class DialogYesNo {
    private TextView textViewTitle;
    public TextView textViewText;
    public Button buttonYes;
    public Button buttonNo;

    public AlertDialog infoDialog(Context context, LayoutInflater layoutInflater, String title, String body, String yes, String no){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View mView = layoutInflater.inflate(R.layout.dialog_yes_or_no, null);
        textViewTitle =  mView.findViewById(R.id.textViewTitle);
        textViewText =  mView.findViewById(R.id.textViewText);
        buttonYes = mView.findViewById(R.id.buttonYes);
        buttonNo = mView.findViewById(R.id.buttonNo);

        textViewTitle.setText(title);
        textViewText.setText(body);
        buttonYes.setText(yes);
        buttonNo.setText(no);

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInfo dialogInfo = new DialogInfo();
                AlertDialog alertDialog = dialogInfo.infoDialog(context, layoutInflater, context.getString(R.string.ERROR), context.getString(R.string.ERROR_NOT_IMPLEMENTED));
                alertDialog.show();
            }
        });

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInfo dialogInfo = new DialogInfo();
                AlertDialog alertDialog = dialogInfo.infoDialog(context, layoutInflater, context.getString(R.string.ERROR), context.getString(R.string.ERROR_NOT_IMPLEMENTED));
                alertDialog.show();
            }
        });

        mBuilder.setView(mView);
        return mBuilder.create();
    }


}
