package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.reservations;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Reservation;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Restaurant;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.RestaurantSchedule;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Schedule;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogInfo;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.dialogs.DialogLoading;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.authentication.LoginActivity;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.instances.UserInfoInstance;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.mainscreen.MainActivity;

public class CreateReservationActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private UserInfoInstance userInfoInstance;

    private AlertDialog dialogLoading;

    private Spinner spinnerRestaurant;
    private Spinner spinnerNumberPeople;
    private TextView textViewDate;
    private TextView textViewTime;
    private Button buttonReserve;
    private Button buttonBack;
    private ImageView imageViewDate;
    private ImageView imageViewTime;

    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;

    private Restaurant currentRestaurant;
    private List<Restaurant> restaurants = new LinkedList<>();
    private RestaurantSchedule restaurantSchedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_reservation);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        calendar = Calendar.getInstance();

        userInfoInstance = UserInfoInstance.getInstance();

        spinnerRestaurant =  findViewById(R.id.spinnerRestaurant);
        spinnerNumberPeople =  findViewById(R.id.spinnerNumberPeople);

        textViewDate =  findViewById(R.id.textViewDate);
        textViewTime =  findViewById(R.id.textViewTime);
        buttonReserve = findViewById(R.id.buttonOrder);
        buttonBack = findViewById(R.id.buttonBack);
        imageViewDate = findViewById(R.id.imageViewDate);
        imageViewTime = findViewById(R.id.imageViewTime);

        dialogLoading = DialogLoading.loadingDialog(CreateReservationActivity.this, getLayoutInflater());

        loadRestaurants();

        imageViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECT_RESTAURANT_FIRST));
            }
        });

        setOnClickListenerTimeNoData();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strTime = textViewTime.getText().toString();
                String strDate = textViewDate.getText().toString();

                if(strTime.equals("")){
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECT_TIME_FIRST));
                    return;
                }

                if(strDate.equals("")){
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECT_DATE_FIRST));
                    return;
                }

                String strNumberPeople = spinnerNumberPeople.getSelectedItem().toString();
                int numberPeople;
                Date dateReserve = null;
                try {

                    dateReserve = new SimpleDateFormat(getString(R.string.Date_format) + " " + getString(R.string.Time_format)).parse(strDate + " " + strTime);
                    numberPeople = Integer.parseInt(strNumberPeople);

                    Date dateNow = new Date(calendar.getTimeInMillis());
                    if(dateReserve.compareTo(dateNow) != 1){
                        popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_CURRENT_DATE_BIGGER_THAN_SELECTED_DATE));
                        return;
                    }

                    buttonReserve.setEnabled(false);
                    dialogLoading.show();
                    Reservation reservation = new Reservation(new Timestamp(dateReserve), user.getUid(), currentRestaurant.getName(), numberPeople, getString(R.string.DATABASE_RESERVATION_WAITING_STATUS_VALUE), userInfoInstance.getUser().getNome(), userInfoInstance.getUser().getTelemovel());
                    db.collection(getString(R.string.DATABASE_RESERVATION_COLLECTION)).add(reservation).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if(task.isSuccessful()){
                                buttonReserve.setEnabled(true);
                                dialogLoading.dismiss();
                                DialogInfo dialogInfo = new DialogInfo();
                                AlertDialog alertDialog = dialogInfo.infoDialog(
                                        CreateReservationActivity.this,
                                        getLayoutInflater(),
                                        getString(R.string.Reserve),
                                        getString(R.string.INFO_RESERVATION_MESSAGE));
                                dialogInfo.buttonOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                        Intent intent = new Intent(CreateReservationActivity.this, ReservationActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                });
                                alertDialog.show();
                            }else{
                                buttonReserve.setEnabled(true);
                                dialogLoading.dismiss();
                                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                            }
                        }
                    });

                } catch (ParseException e) {
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_INVALID_DATE));
                } catch (NumberFormatException e){
                    popupDialog(getString(R.string.ERROR),  getString(R.string.ERROR_INVALID_NUMBER_OF_PEOPLE));
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(user == null){
            Intent intent = new Intent(CreateReservationActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else{
            if(!user.isAnonymous()){
                if(UserInfoInstance.getInstance().getUser() == null){
                    Intent intent = new Intent(CreateReservationActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }else{
                Intent intent = new Intent(CreateReservationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    private void popupDialog(String title, String body){
        DialogInfo dialogInfo = new DialogInfo();
        AlertDialog alertDialog = dialogInfo.infoDialog(CreateReservationActivity.this, getLayoutInflater(), title, body);
        alertDialog.show();
    }

    public void createSpinnerNumberPeople(List<String> values){
        ArrayAdapter<String> adaptarNumberPeople = new ArrayAdapter<>(CreateReservationActivity.this, R.layout.spinner_dropdown_text, values);
        adaptarNumberPeople.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerNumberPeople.setAdapter(adaptarNumberPeople);
    }

    public void loadRestaurants(){
        dialogLoading.show();
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION)).whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        dialogLoading.dismiss();
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                restaurants.add(new Restaurant(document.getId()));
                            }
                            currentRestaurant = restaurants.get(0);
                            LoadSpinnerRestaurants();
                        } else {
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
    }

    private List<String> getRestaurantNames(List<Restaurant> restaurants){
        List<String> restaurantNames = new LinkedList<>();
        for (Restaurant restaurant :restaurants){
            restaurantNames.add(restaurant.getName());
        }
        return restaurantNames;
    }

    public void LoadSpinnerRestaurants(){
        ArrayAdapter<String> adaptarRestaurant = new ArrayAdapter<>(CreateReservationActivity.this, R.layout.spinner_dropdown_text, getRestaurantNames(restaurants));
        adaptarRestaurant.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerRestaurant.setAdapter(adaptarRestaurant);
        spinnerRestaurant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentRestaurant = restaurants.get(position);
                loadSchedules(position);
                textViewDate.setText(getString(R.string.Date));
                textViewTime.setText(getString(R.string.Time));

                db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION)).document(restaurants.get(position).getName()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            int max = Integer.parseInt(task.getResult().get(getString(R.string.DATABASE_MAX_NUMBER_OF_PEOPLE_FIELD)).toString());
                            List<String> values = new LinkedList<>();
                            for (int i = 1; i <= max; i++) {
                                values.add(String.valueOf(i));
                            }
                            createSpinnerNumberPeople(values);
                        }else{
                            popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void loadSchedules(int position){
        dialogLoading.show();
        restaurantSchedule = new RestaurantSchedule();
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION)).document(restaurants.get(position).getName())
                .collection(getString(R.string.DATABASE_SCHEDULE_COLLECTION))
                .whereEqualTo(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD), true)
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> documents = task.getResult().getDocuments();
                    if(documents.isEmpty()){
                        dialogLoading.dismiss();
                        setOnClickListenerDateNoSchedules();
                        setOnClickListenerTimeNoSchedules();
                        return;
                    }

                    for (DocumentSnapshot document : documents) {
                        restaurantSchedule.addDates(new Schedule(
                                document.getString(getString(R.string.DATABASE_WEEKDAY_FIELD)),
                                Integer.parseInt(document.get(getString(R.string.DATABASE_OPENHOUR_FIELD)).toString()),
                                Integer.parseInt(document.get(getString(R.string.DATABASE_CLOSEHOUR_FIELD)).toString())
                        ));
                    }

                    loadSchedulesExceptions(position);
                }else{
                    dialogLoading.dismiss();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });
    }

    public void loadSchedulesExceptions(int position){
        setOnClickListenerTimeNoData();
        setOnClickListenerDateWithSchedules();
        Calendar currentDate = Calendar.getInstance();
        Date dateToday = new Date(currentDate.getTimeInMillis());
        Timestamp today = new Timestamp(dateToday);
        db.collection(getString(R.string.DATABASE_RESTAURANT_COLLECTION)).document(restaurants.get(position).getName())
                .collection(getString(R.string.DATABASE_SCHEDULE_EXCEPTION_COLLECTION))
                .whereGreaterThanOrEqualTo(getString(R.string.DATABASE_END_DATE_FIELD), today)
                .whereEqualTo(getString(R.string.DATABASE_DISABLED_FIELD), false)
                .orderBy(getString(R.string.DATABASE_END_DATE_FIELD))
                .orderBy(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD)) //order by serve para escluir os que nao existem
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> documents = task.getResult().getDocuments();

                    //adicionar as data que vai tar aberto primeiro so depois tratar as escecoes que vai tar fechado
                    for (DocumentSnapshot document : documents) {
                        if(document.get(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD)) == null){
                            continue;
                        }
                        if(!document.getBoolean(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD))){
                            continue;
                        }
                        Calendar start = Calendar.getInstance();
                        start.setTimeInMillis(document.getTimestamp(getString(R.string.DATABASE_START_DATE_FIELD)).toDate().getTime());
                        Calendar end = Calendar.getInstance();
                        end.setTimeInMillis(document.getTimestamp(getString(R.string.DATABASE_END_DATE_FIELD)).toDate().getTime());

                        restaurantSchedule.addToListCalendarDates(start, end);
                    }

                    for (DocumentSnapshot document : documents) {
                        if(document.get(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD)) == null){
                            continue;
                        }
                        if(document.getBoolean(getString(R.string.DATABASE_ACCEPTS_RESERVATIONS_FIELD))){
                            continue;
                        }
                        Calendar start = Calendar.getInstance();
                        start.setTimeInMillis(document.getTimestamp(getString(R.string.DATABASE_START_DATE_FIELD)).toDate().getTime());
                        Calendar end = Calendar.getInstance();
                        end.setTimeInMillis(document.getTimestamp(getString(R.string.DATABASE_END_DATE_FIELD)).toDate().getTime());

                        restaurantSchedule.addClosedExceptions(start, end);
                    }

                    setOnClickListenerTimeNoData();
                    setOnClickListenerDateWithSchedules();
                }else{
                    dialogLoading.show();
                    popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_DEFAULT_MESSAGE));
                }
            }
        });

    }

    public void setOnClickListenerDateNoSchedules(){
        imageViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECTED_RESTAURANT_IS_NOT_OPEN));
            }
        });
    }

    public void setOnClickListenerTimeNoData(){
        imageViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECT_DATE_FIRST));
            }
        });
    }

    public void setOnClickListenerTimeNoSchedules(){
        imageViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog(getString(R.string.ERROR), getString(R.string.ERROR_SELECTED_RESTAURANT_IS_NOT_OPEN));
            }
        });
    }

    public void setOnClickListenerDateWithSchedules(){
        imageViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        textViewDate.setText(String.format(getString(R.string.Date_format_printf), dayOfMonth, monthOfYear+1, year));

                        Calendar calendarDay = Calendar.getInstance();
                        calendarDay.set(year, monthOfYear, dayOfMonth);
                        setOnClickListenerTimeWithSchedules(calendarDay);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.setSelectableDays(restaurantSchedule.getListOpenedDates().toArray(new Calendar[0]));
                datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
            }
        });
        dialogLoading.dismiss();
    }

    public void setOnClickListenerTimeWithSchedules(Calendar calendarDay){
        imageViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        textViewTime.setText(String.format(getString(R.string.Time_format_printf), hourOfDay, minute));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

                int position = restaurantSchedule.getPositionListCalendars(calendarDay);
                if(position != -1) {
                    Timepoint[] selectable = restaurantSchedule.getListOpenedTimes().get(position).getListTimepoint().toArray(new Timepoint[0]);
                    timePickerDialog.setSelectableTimes(selectable);
                }
                timePickerDialog.show(getSupportFragmentManager(), "TimePickerDialog");
            }
        });
    }

}
