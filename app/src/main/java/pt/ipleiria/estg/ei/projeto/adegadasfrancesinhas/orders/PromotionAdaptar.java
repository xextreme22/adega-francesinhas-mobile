package pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.classes.Promotion;
import pt.ipleiria.estg.ei.projeto.adegadasfrancesinhas.R;

public class PromotionAdaptar extends RecyclerView.Adapter<OrderAdaptar.ViewHolder> {
    private List<Promotion> promotions;
    private Context context;
    private int color = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewQuantity;
        public TextView textViewItemName;
        public TextView textViewPrice;
        public RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewItemName = itemView.findViewById(R.id.textViewItemName);
            textViewQuantity = itemView.findViewById(R.id.textViewQuantity);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            relativeLayout = itemView.findViewById(R.id.relativeLayoutItems);
        }
    }

    public PromotionAdaptar(List<Promotion> promotions, Context context) {
        this.promotions = promotions;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderAdaptar.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaptar_item_order_price, parent, false);
        return new OrderAdaptar.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdaptar.ViewHolder holder, int position) {
        Promotion currentPromotion = promotions.get(position);

        holder.textViewItemName.setText(currentPromotion.getMenu());
        String quantity = context.getString(R.string.Quantity_abbreviation) + " " + currentPromotion.getQuantidade();
        holder.textViewQuantity.setText(quantity);
        String price = currentPromotion.getPreco() + " " + context.getString(R.string.Points_abbreviation);
        holder.textViewPrice.setText(price);

        color++;
        switch (color % 3){
            case 0:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow);
                break;
            case 1:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_dark);
                break;
            case 2:
                holder.relativeLayout.setBackgroundResource(R.drawable.shadow_brown_light);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return promotions.size();
    }
}
